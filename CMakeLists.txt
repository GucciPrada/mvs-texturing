cmake_minimum_required(VERSION 3.1)
LIST(APPEND CMAKE_MODULE_PATH "${CMAKE_SOURCE_DIR}/elibs/tbb")

project(Texturing)
include(ExternalProject)

if(NOT CMAKE_BUILD_TYPE)
    message(STATUS "Setting build type to 'RELWITHDEBINFO' as none was specified.")
    set(CMAKE_BUILD_TYPE RELWITHDEBINFO)
endif()

#Search for TBB library (release build)
set(CMAKE_BUILD_TYPE_BACKUP ${CMAKE_BUILD_TYPE})
set(CMAKE_BUILD_TYPE RELEASE)
#set(TBB_INCLUDE_DIRS "/home/zm/Downloads/tbb2018_20180618oss_lin/tbb2018_20180618oss/include")
#set(TBB_LIBRARY "/home/zm/Downloads/tbb2018_20180618oss_lin/tbb2018_20180618oss/lib/intel64/gcc4.7")
find_package(TBB COMPONENTS
#tbbmalloc_proxy tbbmalloc
tbb REQUIRED)
set(CMAKE_BUILD_TYPE ${CMAKE_BUILD_TYPE_BACKUP})

#add_definitions(-DEIGEN_MPL2_ONLY)
set(CMAKE_EXE_LINKER_FLAGS "${CMAKE_EXE_LINKER_FLAGS} -v -pthread")
set(CMAKE_CXX_STANDARD 11)
set(CMAKE_CXX_STANDARD_REQUIRED ON)
#set(CMAKE_CXX_EXTENSIONS OFF)

#OpenMP版本的llvm
#FIND_PACKAGE(OpenMP)
if(OPENMP_FOUND)
    set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} ${OpenMP_C_FLAGS}")
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} ${OpenMP_CXX_FLAGS}")
    set(CMAKE_EXE_LINKER_FLAGS "${CMAKE_EXE_LINKER_FLAGS} ${OpenMP_EXE_LINKER_FLAGS}")
endif()

if(CMAKE_COMPILER_IS_GNUCXX)
    #gnu gcc设置native可以开启__AVX__等宏
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS}  -v -Wall -Wextra -Wundef -pedantic")
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -mfpmath=sse -funroll-loops")
endif()

IF(${CMAKE_CXX_COMPILER_ID} STREQUAL "AppleClang")
    SET(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -mavx -march=core-avx2 -v -Wall -Wextra -Wundef -W#warnings -pedantic -W#pragma-messages -Wignored-pragmas -Wunknown-pragmas")
ENDIF()

find_package(Eigen3  REQUIRED)
#set(PNG_LIBRARY "")
find_package(PNG REQUIRED)
#find_package(JPEG REQUIRED)
#find_package(TIFF REQUIRED)
find_package(OpenCV 3 REQUIRED core imgproc imgcodecs highgui photo )
find_package(Boost REQUIRED)

#add_subdirectory(elibs)
#指定头文件搜索目录
include_directories(SYSTEM
    ${CMAKE_SOURCE_DIR}/elibs/rayint/libs
    ${CMAKE_SOURCE_DIR}/elibs/mve/libs
    ${EIGEN3_INCLUDE_DIR}
    ${CMAKE_SOURCE_DIR}/elibs/mapmap/
    ${CMAKE_SOURCE_DIR}/elibs/mapmap/mapmap
    ${Boost_INCLUDE_DIRS}
    ${CMAKE_SOURCE_DIR}/elibs
)
include_directories(
    libs
)

#指定库文件搜索目录
link_directories(
    ${CMAKE_SOURCE_DIR}/elibs/mve/libs/mve #libmve.a
    ${CMAKE_SOURCE_DIR}/elibs/mve/libs/util #libmve_util.a
)

add_subdirectory(libs)
add_subdirectory(apps)
// TexException.hpp
// Created by chenglong on 2018/8/17.
// Copyright © 2018 www.zhimei.ai All rights reserved.
//
#ifndef TEXEXCEPTION_H
#define TEXEXCEPTION_H
#include <sstream>
#include <string>
#include "tex/defines.h"


//错误码区段间隔
#define CODE_SEGMENT 10000
//关键错误码的起点
#define CRITICAL_BEGIN 60000
//非关键错误码的起点
#define NON_CRITICAL_BEGIN 70000

TEX_NAMESPACE_BEGIN

/**
 *
 * @brief
 * 错误码枚举.
 * 分为关键错误码(code for critical error)和非关键错误码
 * @details
 * 1.编码约定:
 *
 *   关键错误码以40000打头,非关键错误以50000打头.
 *
 * 2.命名约定:
 *
 *    E_[ModuleShortName]_[Operation]_[detail]
 *
 * 其中E_前缀代表这是一个错误码，
 * ModuleShortName是模块名字缩写，比如KF代表关键帧相关的模块；
 * Operation代表该模块内的具体操作，比如读/写/计算/转换/生成/查找等等，
 * detail可以是任意有意义的东西，通常是Operation的操作客体或称为操作对象，比
 * 如图片、相机参数、三角网格、体素、文件等等数据对象.当然也可以是其他的对象，
 * 也可以是其他名字.
 *
 * 3.使用约定:
 *
 *   关键错误码用在不可恢复不可挽救的地方,非关键错误码用在出
 * 现了不影响主要功能的错误或异常的地方。关键错误码都有对应的预定义异常对象(
 * tex::TexException实例),可通过
 * TexException::getPredefinedByCode(int)获取预定义的异常对象.
 * 建议在发生关键错误的地点抛出预定义的异常.例如
 *
 * \code
 * throw TexException::getPredefinedByCode(E_CLEAN_WRITE_Polyhedron)
          .setLogMsg("write polyhedron failed");
  \endcode
 *
 *
 * @note c++中枚举转int是自动的,但int转枚举须显式地强转
 * @note 这个文件的定义与zmscan中的BaseException和ErrorCodeEnum相似
 *
 */
enum TexErrorCodeEnum {

  UNKOWN_ERROR = -1,
  NO_ERROR = 0,

  //====关键错误====

  /*tracker连续失败多次*/
  E_Track_TooMuchInvalidPose = CRITICAL_BEGIN,
  /*关键帧的彩色图片数据有问题*/
  E_KF_DATA_image=CRITICAL_BEGIN+1,
  /*关键帧的深度图数据有问题*/
  E_KF_DATA_depth=CRITICAL_BEGIN+2,
  /*关键帧的特征点数据有问题*/
  E_KF_DATA_ldmk=CRITICAL_BEGIN+3,
  /*关键帧的相机参数有问题*/
  E_KF_DATA_cam=CRITICAL_BEGIN+4,
  /*关键帧的三维特征顶点数据有问题*/
  E_KF_DATA_ldmk3d=CRITICAL_BEGIN+5,

  /*关键帧-彩图读取出错*/
  E_KF_READ_image=CRITICAL_BEGIN+6,
  /*关键帧-深度图读取出错*/
  E_KF_READ_depth=CRITICAL_BEGIN+7,
  /*关键帧-二维特征点读取出错*/
  E_KF_READ_ldmk=CRITICAL_BEGIN+8,
  /*关键帧-相机参数读取出错*/
  E_KF_READ_cam=CRITICAL_BEGIN+9,
  /*关键帧-三维特征顶点读取出错*/
  E_KF_READ_ldmk3d=CRITICAL_BEGIN+10,

  /*关键帧-彩图保存出错*/
  E_KF_WRITE_image=CRITICAL_BEGIN+11,
  /*关键帧-深度图保存出错*/
  E_KF_WRITE_depth=CRITICAL_BEGIN+12,
  /*关键帧-二维特征点保存出错*/
  E_KF_WRITE_ldmk=CRITICAL_BEGIN+13,
  /*关键帧-相机参数保存出错*/
  E_KF_WRITE_cam=CRITICAL_BEGIN+14,
  /*关键帧-三维特征点保存出错*/
  E_KF_WRITE_ldmk3d=CRITICAL_BEGIN+15,

  /*无法为.cam文件找到配对的.png和.yml文件*/
  E_KF_cam_no_match=CRITICAL_BEGIN+16,

  /*[mesh清理]-加载obj到多面体出错*/
  E_CLEAN_READ_Polyhedron=CRITICAL_BEGIN+29,
  /*[mesh清理]-保存多面体到obj文件出错*/
  E_CLEAN_WRITE_Polyhedron=CRITICAL_BEGIN+30,

  /*[mesh裁剪]-mesh裁剪异常*/
  E_CLEAN_CLIP=CRITICAL_BEGIN+31,
  /*[mesh人头转正]-mesh人头转正异常*/
  E_RAYINT_FRONT=CRITICAL_BEGIN+32,

  /*[三维特征顶点] - 关键帧数据不完整, 数量不对*/
  E_3DLdmk_KFData_Incomplete=CRITICAL_BEGIN+41,
  /*[三维特征顶点] - 缺少相机参数*/
  E_3DLdmk_KFData_cam=CRITICAL_BEGIN+42,
  /*[三维特征顶点] - 缺少深度图*/
  E_3DLdmk_KFData_dep=CRITICAL_BEGIN+43,
  /*[三维特征顶点] - 缺少二维特征点*/
  E_3DLdmk_KFData_ldmk2d=CRITICAL_BEGIN+44,
  /*[三维特征顶点] - 找不到关键的特征点 - 用于贴纹理的几个特征点查找失败*/
  E_3DLdmk_KeyPoint_Missing=CRITICAL_BEGIN+45,
  /*[三维特征顶点] - 最终计算出来的交点集合个数少于3*/
  E_3DLdmk_NotEnough=CRITICAL_BEGIN+46,

  //====非关键错误====

  /*参数错误*/
  E_RANGE_PARAMS_ERROR = NON_CRITICAL_BEGIN,
  /* Range Image Segmentation*/
  /*人脸框(由于人脸离屏幕太远)太小, 或(由于人脸离屏幕太近)太大*/
  E_RANGE_RectTooSmallOrBig = NON_CRITICAL_BEGIN+1,
  /*头部距离屏幕太近*/
  E_RANGE_TooClose = NON_CRITICAL_BEGIN+2,
  /*头部距离屏幕太远*/
  E_RANGE_TooFar = NON_CRITICAL_BEGIN+3,
  /*在人脸框的中心点附近20x20邻域内找不到种子点*/
  E_RANGE_NoSeedOnFace = NON_CRITICAL_BEGIN+4,
  /*在人脸框的下沿，找不到脖子上的种子点*/
  E_RANGE_NoSeedOnNeck = NON_CRITICAL_BEGIN+5,
  /* 深度图分割后，有效像素数量太少了 */
  E_RANGE_PixelNotEnough = NON_CRITICAL_BEGIN+6,

  /* 后期脖子深度图分割时，发现人脸框太小或太大 */
  E_NECK_RectTooSmallOrBig=NON_CRITICAL_BEGIN+7,
  /* 后期脖子深度图分割时，发现下颌box内没有有效像素 */
  E_NECK_NoPixel=NON_CRITICAL_BEGIN+8,
  /* 后期脖子深度图分割时，在脖子上找不到种子点*/
  E_NECK_NoSeedOnNeck=NON_CRITICAL_BEGIN+9,

};

/**
 * 基础异常.
 */
class TexException {
private:
  /** 错误码
   * @see
   */
  TexErrorCodeEnum code;
  /**
   * 给用户看的消息/文案
   */
  std::string uimsg;

  /**
   * 日志消息，包含现场、参数信息. 通常debug模式下可见.
   */
  std::string logmsg;

public:
  TexException(TexErrorCodeEnum code,
                std::string uimsg = "",
                std::string logmsg = "")
      : code(code), uimsg(uimsg), logmsg(logmsg) {}
  virtual ~TexException(){};
  //是否为关键错误
  virtual bool isCriticalException() { return true; }
  virtual TexErrorCodeEnum getCode() const { return code; }
  virtual TexException &setCode(TexErrorCodeEnum code) {
    this->code = code;
    return *this;
  }
  virtual TexException &setUIMsg(std::string uimsg) {
    this->uimsg = uimsg;
    return *this;
  }
  virtual std::string getUIMsg() const{ return uimsg; }
  virtual TexException &setLogMsg(std::string logmsg) {
    this->logmsg = logmsg;
    return *this;
  }
  virtual std::string getLogMsg() const { return logmsg; }
  //获取该异常的错误码+描述消息
  virtual std::string toString() const{
    if (code == NO_ERROR) {
      return "操作成功";
    }
    std::ostringstream oss;
#ifdef DEBUG
    oss << "错误码:" << code << ",msg:" << logmsg << ",uimsg:" << uimsg;
#else
    oss << uimsg << "(错误码:" << (int)code << ")";
#endif
    return oss.str();
  }

  //根据错误码来获取预定义的错误消息
  static std::string getPredefinedMsgByCode(int code);
  //根据错误码来获取预定义的错误类型
  static TexException getPredefinedByCode(int code);
};

//关键错误:4xxxx
static bool isCriticalException(int exceptionCode) {
  const int r = (exceptionCode - CRITICAL_BEGIN);
  return 0 <= r && r < CODE_SEGMENT;
}

static bool isCriticalException(TexErrorCodeEnum expEnum) {
  return isCriticalException((int)expEnum);
}

static bool isCriticalException(TexException &texException) {
  return isCriticalException(texException.getCode());
}

TEX_NAMESPACE_END

#endif //TEXEXCEPTION_H
// TexException.cpp
// Created by chenglong on 2018/8/17.
// Copyright © 2018 www.zhimei.ai All rights reserved.
//
#include "TexException.h"
#include <map>

TEX_NAMESPACE_BEGIN
/**
 * 预定义的错误消息.(关键错误)
 */
static const std::map<int, TexException> predefined {
    /* clang-format off */
    {UNKOWN_ERROR, TexException(UNKOWN_ERROR, "未知错误")},
    {E_KF_READ_ldmk, TexException(E_KF_READ_ldmk, "特征点文件读取失败")},
    {E_KF_DATA_ldmk, TexException(E_KF_DATA_ldmk,"特征点文件数据不完整")},
    {E_KF_READ_cam,TexException(E_KF_READ_cam,"相机参数读取失败")},
    {E_KF_DATA_cam,TexException(E_KF_DATA_cam,"相机参数数据不完整")},
    {E_KF_cam_no_match,TexException(E_KF_cam_no_match,"找不到与相机参数匹配的数据")},
    /* clang-format on */
};

TexException TexException::getPredefinedByCode(int code) {
  try {
    return predefined.at(code);
  } catch (std::out_of_range e) {
    return predefined.at((int)UNKOWN_ERROR);
  }
}

std::string TexException::getPredefinedMsgByCode(int code) {
  return getPredefinedByCode(code).toString();
}

TEX_NAMESPACE_END

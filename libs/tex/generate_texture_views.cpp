/*
 * Copyright (C) 2015, Nils Moehrle
 * TU Darmstadt - Graphics, Capture and Massively Parallel Computing
 * All rights reserved.
 *
 * This software may be modified and distributed under the terms
 * of the BSD 3-Clause license. See the LICENSE.txt file for details.
 */

#include <mve/bundle_io.h>
#include <mve/image_io.h>
#include <mve/image_tools.h>
#include <mve/scene.h>
#include <util/timer.h>
#include <util/tokenizer.h>
#include <exception/TexException.h>

#include "progress_counter.h"
#include "texturing.h"
#include "view_io.h"


TEX_NAMESPACE_BEGIN

void from_mve_scene(std::string const &scene_dir,
                    std::string const &image_name,
                    std::vector<TextureView> *texture_views) {

  mve::Scene::Ptr scene;
  try {
    scene = mve::Scene::create(scene_dir);
  } catch (std::exception &e) {
    std::cerr << "Could not open scene: " << e.what() << std::endl;
    std::exit(EXIT_FAILURE);
  }
  std::size_t num_views = scene->get_views().size();
  texture_views->reserve(num_views);

  ProgressCounter view_counter("\tLoading", num_views);
  for (std::size_t i = 0; i < num_views; ++i) {
    view_counter.progress<SIMPLE>();

    mve::View::Ptr view = scene->get_view_by_id(i);
    if (view == NULL) {
      view_counter.inc();
      continue;
    }

    if (!view->has_image(image_name, mve::IMAGE_TYPE_UINT8)) {
      std::cout << "Warning: View " << view->get_name() << " has no byte image "
                << image_name << std::endl;
      continue;
    }

    mve::View::ImageProxy const *image_proxy =
        view->get_image_proxy(image_name);

    if (image_proxy->channels < 3) {
      std::cerr << "Image " << image_name << " of view " << view->get_name()
                << " is not a color image!" << std::endl;
      exit(EXIT_FAILURE);
    }

    texture_views->push_back(
        TextureView(view->get_id(), view->get_camera(),
                    util::fs::abspath(util::fs::join_path(
                        view->get_directory(), image_proxy->filename))));
    view_counter.inc();
  }
}

void from_images_and_camera_files(std::string const &path,
                                  std::vector<TextureView> *texture_views,
                                  std::string const &tmp_dir) {

  util::fs::Directory dir(path);
  std::map<std::string,util::fs::File*> filesMap;
  for(auto it=dir.begin(); it!=dir.end(); it++){
    if (it->name.empty() || it->name[0]=='.' ||it->is_dir ) continue;
    filesMap[it->name] = &(*it);
  }
  //==>填充files数组:0.cam,0.png,0.yml,1.cam,1.png,1.yml,2.cam,2.png,2.yml...
  std::vector<std::string> files;
  for (auto i = filesMap.begin(); i != filesMap.end(); ++i) {
    const std::string &cam_file = i->first;
    std::string cam_file_ext =
        util::string::uppercase(util::string::right(cam_file, 4));
    if (cam_file_ext != ".CAM")
      continue;

    std::string prefix =
        util::string::left(cam_file, cam_file.size() - 4);
    if (prefix.empty()) continue;

    /* Find corresponding image file.
     * 顺便找到特征点文件.要求它们的前缀与cam文件的前缀一样.
     * 从xx.cam后面开始搜索.图片暂时只用png.*/
     auto ldmkIt=filesMap.find(prefix+".yml");
     auto imgIt=filesMap.find(prefix+".png");
    if (ldmkIt==filesMap.end() || imgIt==filesMap.end()){
      std::cerr<<"cannot find matches for "<<i->second->get_absolute_name()<<std::endl;
      throw TexException::getPredefinedByCode(TexErrorCodeEnum::E_KF_cam_no_match);
    }
    files.push_back(i->second->get_absolute_name());
    files.push_back(imgIt->second->get_absolute_name());
    files.push_back(ldmkIt->second->get_absolute_name());
  }//end for filesMap.begin()

  //==>加载files数组中的数据
  constexpr int FILE_COUNT_PER_VIEW=3;
  ProgressCounter view_counter("\tLoading", files.size() / FILE_COUNT_PER_VIEW);
//#pragma omp parallel for
  for (std::size_t i = 0; i < files.size(); i += FILE_COUNT_PER_VIEW) {
    view_counter.progress<SIMPLE>();
    const std::string &cam_file = files[i];
    const std::string &img_file = files[i + 1];
    const std::string &ldmk_file = files[i+2];

    /* 读取ZMScan输出的相机参数文件*/
    mve::CameraInfo cam_info;
    readCameraFile(cam_file, cam_info);

//todo:#pragma omp critical 临界区?
    //获取彩色图片，看情况去畸变
    std::string image_file = util::fs::abspath(img_file);
    //如果畸变参数不为0则现在就纠正图片并将纠正后的图像写入
    if (cam_info.dist[0] != 0.0f) {
      mve::ByteImage::Ptr image = mve::image::load_file(img_file);
      if (cam_info.dist[1] != 0.0f) {
        image = mve::image::image_undistort_k2k4<uint8_t>(
            image, cam_info.flen, cam_info.dist[0], cam_info.dist[1]);
      } else {
        image = mve::image::image_undistort_vsfm<uint8_t>(image, cam_info.flen,
                                                          cam_info.dist[0]);
      }
      //将畸变校正之后的彩色图写入到临时目录
      image_file = util::fs::join_path(tmp_dir,
          util::fs::replace_extension(util::fs::basename(img_file), "png"));
      mve::image::save_png_file(image, image_file);
    }
    //读取二维特征点
    float pitch,yaw,roll;
    TextureView::Landmark2DArrPtr ldmk2d;
    std::tie(pitch,yaw,roll)=readLdmk2dArr(ldmk_file,ldmk2d);

//todo:#pragma omp critical 临界区?
    texture_views->push_back(TextureView(i / FILE_COUNT_PER_VIEW, cam_info, image_file));
    texture_views->back().set_ldmk2darr(ldmk2d);
    texture_views->back().set_pitch(pitch);
    texture_views->back().set_yaw(yaw);
    texture_views->back().set_roll(roll);
    view_counter.inc();
  }//end for i<files.size()
}

void from_nvm_scene(std::string const &nvm_file,
                    std::vector<TextureView> *texture_views,
                    std::string const &tmp_dir) {
  std::vector<mve::NVMCameraInfo> nvm_cams;
  mve::Bundle::Ptr bundle = mve::load_nvm_bundle(nvm_file, &nvm_cams);
  mve::Bundle::Cameras &cameras = bundle->get_cameras();

  ProgressCounter view_counter("\tLoading", cameras.size());
//#pragma omp parallel for
  for (std::size_t i = 0; i < cameras.size(); ++i) {
    view_counter.progress<SIMPLE>();
    mve::CameraInfo &mve_cam = cameras[i];
    mve::NVMCameraInfo const &nvm_cam = nvm_cams[i];

    mve::ByteImage::Ptr image = mve::image::load_file(nvm_cam.filename);

    int const maxdim = std::max(image->width(), image->height());
    mve_cam.flen = mve_cam.flen / static_cast<float>(maxdim);

    image = mve::image::image_undistort_vsfm<uint8_t>(
        image, mve_cam.flen, nvm_cam.radial_distortion);

    const std::string image_file = util::fs::join_path(
        tmp_dir, util::fs::replace_extension(
                     util::fs::basename(nvm_cam.filename), "png"));
    mve::image::save_png_file(image, image_file);

//#pragma omp critical
    texture_views->push_back(TextureView(i, mve_cam, image_file));

    view_counter.inc();
  }
}

void generate_texture_views(std::string const &in_scene,
                            std::vector<TextureView> *texture_views,
                            std::string const &tmp_dir) {
  /* Determine input format. */

  /* BUNDLEFILE */
  if (util::fs::file_exists(in_scene.c_str())) {
    std::string const &file = in_scene;
    std::string extension =
        util::string::uppercase(util::string::right(file, 3));
    if (extension == "NVM") {
      from_nvm_scene(file, texture_views, tmp_dir);
    }
  }

  /* SCENE_FOLDER */
  if (util::fs::dir_exists(in_scene.c_str())) {
    from_images_and_camera_files(in_scene, texture_views, tmp_dir);
  }

  /* MVE_SCENE::EMBEDDING */
  size_t pos = in_scene.rfind("::");
  if (pos != std::string::npos) {
    std::string scene_dir = in_scene.substr(0, pos);
    std::string image_name = in_scene.substr(pos + 2, in_scene.size());
    from_mve_scene(scene_dir, image_name, texture_views);
  }

  std::sort(texture_views->begin(), texture_views->end(),
            [](TextureView const &l, TextureView const &r) -> bool {
              return l.get_id() < r.get_id();
            });

  std::size_t num_views = texture_views->size();
  if (num_views == 0) {
    std::cerr << "No proper input scene descriptor given.\n"
              << "A input descriptor can be:\n"
              << "BUNDLE_FILE - a bundle file (currently onle .nvm files are "
                 "supported)\n"
              << "SCENE_FOLDER - a folder containing images and .cam files\n"
              << "MVE_SCENE::EMBEDDING - a mve scene and embedding\n"
              << std::endl;
    exit(EXIT_FAILURE);
  }
}

TEX_NAMESPACE_END

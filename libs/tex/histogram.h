/*
 * Copyright (C) 2015, Nils Moehrle
 * TU Darmstadt - Graphics, Capture and Massively Parallel Computing
 * All rights reserved.
 *
 * This software may be modified and distributed under the terms
 * of the BSD 3-Clause license. See the LICENSE.txt file for details.
 */

#ifndef TEX_HISTOGRAM_HEADER
#define TEX_HISTOGRAM_HEADER

#include <string>
#include <vector>
#include "defines.h"

TEX_NAMESPACE_BEGIN
/**
 * Class representing a histogram with a fixed number of bins
 * optimized to calculate approximate permilles.
 * 固定bin数目的柱状图. 优化到能够计算千分位的精确度.
 */
class Histogram {
private:
  std::vector<unsigned int> bins;
  ///<最小样本值
  float min;
  ///<最大样本值
  float max;
  ///< 样本总数
  int num_values;

public:
  /** Constructs a histogram with num_bins bins which clamps values to [_min,
   * _max]. */
  Histogram(float _min, float _max, std::size_t num_bins);

  /** Adds a value to the histogram. The value is clamped to [min, max]. */
  void add_value(float value);

  /**
   * Saves the histogram to a .csv file.
   * @throws util::FileException
   */
  void save_to_file(std::string const &filename) const;

  /**
   * Returns the approximate permille.
   * 给定一个百分比percentile，在柱状图中找到一个区间取值的上限，使得低于该上限值的元素总数
   * 占总样本数不少于percentile.
   */
  float get_approx_percentile(float percentile) const;

  int get_num_values() const {
    return this->num_values;
  }
};

TEX_NAMESPACE_END

#endif /* TEX_HISTOGRAM_HEADER */

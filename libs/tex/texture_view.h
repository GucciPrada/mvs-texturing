/*
 * Copyright (C) 2015, Nils Moehrle
 * TU Darmstadt - Graphics, Capture and Massively Parallel Computing
 * All rights reserved.
 *
 * This software may be modified and distributed under the terms
 * of the BSD 3-Clause license. See the LICENSE.txt file for details.
 */

#ifndef TEX_TEXTUREVIEW_HEADER
#define TEX_TEXTUREVIEW_HEADER

#include <string>
#include <vector>

#include <math/vector.h>
#include <mve/camera.h>
#include <mve/image.h>

#include "settings.h"
#include "tri.h"

TEX_NAMESPACE_BEGIN

/** Struct containing the quality and mean color of a face within a view.
 * 单个三角面的投影信息:所投影的视图id、quality(gmi/area)、平均颜色向量
 */
struct FaceProjectionInfo {
  ///< 关联的label
  std::uint16_t view_id;
  /**
   * gmi或者area.正常计算出来的都是非负数
   */
  float quality;
  /**
   * 保存的是三角面在某个视图中投影结果中所有像素样本的三通道颜色平
   * 均值,每个通道单独计算总和,再除以样本总数目,可用于照片一致性检查.
   */
  math::Vec3f mean_color;

  bool operator<(FaceProjectionInfo const &other) const {
    return view_id < other.view_id;
  }
};

/**
 * Class representing a view with specialized functions for texturing.
 */
class TextureView {

  public:
  //单个视图中的2d特征点序列
  typedef std::vector<math::Vec2f> Landmark2DArr;
  //单个视图中的2d特征点序列
  typedef std::shared_ptr<Landmark2DArr> Landmark2DArrPtr;

private:
  /// 视图编号
  std::size_t id;
  /// 相机光心在世界中的坐标
  math::Vec3f pos;
  /// 相机光轴在世界中的方向向量
  math::Vec3f viewdir;
  /// 相机坐标->图像坐标
  math::Matrix3f projection;
  /// 世界坐标->相机坐标
  math::Matrix4f world_to_cam;
  int width;
  int height;
  /// 图片文件路径
  std::string image_file;
  /// mve图像对象，包含图片的数据内容
  mve::ByteImage::Ptr image;
  /// 图片的梯度场, 每个像素的亮度通道，都有一个梯度值，若原始图有alpha通道则还会计算此通道的梯度,
  /// 代表的是sobel算子计算出的梯度向量的长度(幅度), 最大值不超过255.
  mve::ByteImage::Ptr gradient_magnitude;
  /// 像素掩码图? todo:可以考虑使用bitset,因为图像尺寸是编译时已知且固定的
  std::vector<bool> validity_mask;
  //todo:加上特征点和欧拉角
  Landmark2DArrPtr ldmk2ds;
  float pitch,yaw,roll;
public:

  /** Returns the id of the TexureView which is consistent for every run. */
  std::size_t get_id() const;

  /** Returns the 2D pixel coordinates of the given vertex projected into the
   * view. */
  math::Vec2f get_pixel_coords(math::Vec3f const &vertex) const;

  /** Returns the RGB pixel values [0, 1] for the given vertex projected into
   * the view, calculated by linear interpolation. */
  math::Vec3f get_pixel_values(math::Vec3f const &vertex) const;

  /** Returns the RGB pixel values [0, 1] for the give pixel location. */
  math::Vec3f get_pixel_values(math::Vec2f const &pixel) const;

  /** Returns whether the pixel location is valid in this view.
   * The pixel location is valid if its inside the visible area and,
   * if a validity mask has been generated, all surrounding (integer coordinate)
   * pixels are valid in the validity mask.
   *
   */
  bool valid_pixel(math::Vec2f pixel) const;

  /** TODO */
  bool inside(math::Vec3f const &v1,
              math::Vec3f const &v2,
              math::Vec3f const &v3) const;



  /** Constructs a TextureView from the give mve::CameraInfo containing the
   * given image. */
  TextureView(std::size_t id,
              mve::CameraInfo const &camera,
              std::string const &image_file);

  /** Returns the position. */
  math::Vec3f get_pos() const;
  /** Returns the viewing direction. */
  math::Vec3f get_viewing_direction() const;
  /** Returns the width of the corresponding image. */
  int get_width() const;
  /** Returns the height of the corresponding image. */
  int get_height() const;
  /** Returns a reference pointer to the corresponding image. */
  mve::ByteImage::Ptr get_image() const;
  void set_pitch(float pitch){this->pitch=pitch;}
  float get_pitch()const {return pitch;}
  void set_yaw(float yaw){this->yaw=yaw;}
  float get_yaw()const {return yaw;}
  void set_roll(float roll){this->roll=roll;}
  float get_roll()const{return roll;}
  Landmark2DArrPtr get_ldmk2darr()const {return ldmk2ds;}
  void set_ldmk2darr(const Landmark2DArrPtr &ldmk2darr){this->ldmk2ds=ldmk2darr;}

  /** change encapsulated image. */
  void bind_image(mve::ByteImage::Ptr new_image);

  /** Loads the corresponding image. */
  void load_image();
  /** Generates the validity mask. */
  void generate_validity_mask();
  /** Generates the gradient magnitude image for the encapsulated image.
   * note:要求TextureView中的图像是RGB或RGBA.
   * 输入的三通道RGB图转到HSV(使用mve::image::DESATURATE_LUMINANCE desaturate)，
   * 在luminance通道上计算梯度图.若输入的是四通道RGBA，则生成的梯度图有两个通道，#0通道保存
   * 的是亮度梯度, #1通道保存的是原始alpha通道的梯度.
   **/
  void generate_gradient_magnitude();

  /** Releases the validity mask. */
  void release_validity_mask();
  /** Releases the gradient magnitude image. */
  void release_gradient_magnitude();
  /** Releases the corresponding image. */
  void release_image();

  /** Erodes the validity mask by one pixel. */
  void erode_validity_mask();

  void get_face_info(math::Vec3f const &v1,
                     math::Vec3f const &v2,
                     math::Vec3f const &v3,
                     FaceProjectionInfo *face_info,
                     Settings const &settings) const;

  void export_triangle(math::Vec3f v1,
                       math::Vec3f v2,
                       math::Vec3f v3,
                       std::string const &filename) const;

  void export_validity_mask(std::string const &filename) const;
};//end class TextureView

inline std::size_t TextureView::get_id() const { return id; }
//相机光心在世界中的坐标
inline math::Vec3f TextureView::get_pos() const { return pos; }

inline math::Vec3f TextureView::get_viewing_direction() const {
  return viewdir;
}

inline int TextureView::get_width() const { return width; }

inline int TextureView::get_height() const { return height; }

inline mve::ByteImage::Ptr TextureView::get_image() const {
  assert(image != NULL);
  return image;
}

inline bool TextureView::inside(math::Vec3f const &v1,
                                math::Vec3f const &v2,
                                math::Vec3f const &v3) const {
  math::Vec2f p1 = get_pixel_coords(v1);
  math::Vec2f p2 = get_pixel_coords(v2);
  math::Vec2f p3 = get_pixel_coords(v3);
  return valid_pixel(p1) && valid_pixel(p2) && valid_pixel(p3);
}

inline math::Vec2f
TextureView::get_pixel_coords(math::Vec3f const &vertex) const {
  math::Vec3f pixel = projection * world_to_cam.mult(vertex, 1.0f);
  pixel /= pixel[2];
  //note:减0.5,真实世界的成像点是落在像素中心，而本算法内部整数像素坐标都是方形像素左上角
  return math::Vec2f(pixel[0] - 0.5f, pixel[1] - 0.5f);
}

inline math::Vec3f
TextureView::get_pixel_values(math::Vec3f const &vertex) const {
  math::Vec2f pixel = get_pixel_coords(vertex);
  return get_pixel_values(pixel);
}

inline math::Vec3f
TextureView::get_pixel_values(math::Vec2f const &pixel) const {
  assert(image != NULL);
  math::Vec3uc values;
  image->linear_at(pixel[0], pixel[1], *values);
  return math::Vec3f(values) / 255.0f;
}

inline void TextureView::bind_image(mve::ByteImage::Ptr new_image) {
  image = new_image;
}

inline void TextureView::release_validity_mask() {
  assert(validity_mask.size() == static_cast<std::size_t>(width * height));
  validity_mask = std::vector<bool>();
}

inline void TextureView::release_gradient_magnitude() {
  assert(gradient_magnitude != NULL);
  gradient_magnitude.reset();
}

inline void TextureView::release_image() {
  assert(image != NULL);
  image.reset();
}

TEX_NAMESPACE_END

#endif /* TEX_TEXTUREVIEW_HEADER */

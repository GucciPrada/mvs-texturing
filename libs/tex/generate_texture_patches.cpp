/*
 * Copyright (C) 2015, Nils Moehrle
 * TU Darmstadt - Graphics, Capture and Massively Parallel Computing
 * All rights reserved.
 *
 * This software may be modified and distributed under the terms
 * of the BSD 3-Clause license. See the LICENSE.txt file for details.
 */

#include <list>
#include <set>

#include <Eigen/SparseCore>
#include <Eigen/SparseLU>
#include <mve/image_tools.h>
#include <util/timer.h>

#include "texturing.h"

TEX_NAMESPACE_BEGIN

#define MAX_HOLE_NUM_FACES 100
#define MAX_HOLE_PATCH_SIZE 100

template <typename T>
T clamp_nan_low(T const &v, T const &lo, T const &hi) {
  return (v > lo) ? ((v < hi) ? v : hi) : lo;
}

template <typename T>
T clamp_nan_hi(T const &v, T const &lo, T const &hi) {
  return (v < hi) ? ((v > lo) ? v : lo) : hi;
}

template <typename T>
T clamp(T const &v, T const &lo, T const &hi) {
  return (v < lo) ? lo : ((v > hi) ? hi : v);
}

/**
 * 合并顶点投影信息邻接表.相同texture_patch_id的VPI合并为一个VPI.
  +--+    +---------------------+
  |V0+----+VPI:0,(u0,v0),{F0,F1}+ 相同patch的关联面放在一起
  |  |    +---------------------+
  |V1|    |VPI:1,(u0,v0),{F3}   + 不同patch,纹理坐标相同,可能来自相同视图
  |  | .  +---------------------+
  |  | .  |VPI:3,(u1,v1),{F6}   + 不同patch,不同纹理坐标,来自不同视图
  |  | .  +---------------------+
  |..| .
  |  |
  |  |
  |Vn|
  +--+
 * @param vertex_projection_infos
 */
void merge_vertex_projection_infos(
    std::vector<std::vector<VertexProjectionInfo>> *vertex_projection_infos) {
/* Merge vertex infos within the same texture patch. */
//#pragma omp parallel for
  for (std::size_t i = 0; i < vertex_projection_infos->size(); ++i) {
    std::vector<VertexProjectionInfo> &infos = vertex_projection_infos->at(i);

    std::map<std::size_t, VertexProjectionInfo> info_map;
    std::map<std::size_t, VertexProjectionInfo>::iterator it;

    for (VertexProjectionInfo const &info : infos) {
      std::size_t texture_patch_id = info.texture_patch_id;
      if ((it = info_map.find(texture_patch_id)) == info_map.end()) {
        info_map[texture_patch_id] = info;
      } else {
        it->second.faces.insert(it->second.faces.end(), info.faces.begin(),
                                info.faces.end());
      }
    }

    infos.clear();
    infos.reserve(info_map.size());
    for (it = info_map.begin(); it != info_map.end(); ++it) {
      infos.push_back(it->second);
    }
  }
}

/** Struct representing a TexturePatch candidate
 * - final texture patches are obtained by merging candiates.
 * - 一个candidate代表的是一个视图中的联通片段,一个视图中可能贡献好几个联通子图,
 *   因此从一个视图中可能可以提取多个patch candidate.
 * */
struct TexturePatchCandidate {
  int cand_id;///<单个视图内部的candidate id
  /**
   * 此candidate对应的图像片段在视图中的bbox
   */
  Rect<int> bounding_box;
  /**
   * 此candidate对应的patch:包含联通子图、子图中三角面的每个
   * 顶点的纹理坐标、图像片段、
   */
  TexturePatch::Ptr texture_patch;
};

 /**
  * Create a TexturePatchCandidate by calculating the faces' bounding box
  * projected into the view, relative texture coordinates and extacting
  * the texture views relevant part.
  *
  * 创建一个patch candidate, 它代表的是mesh中被MRF分割出来的一个联通子图构成的三角面片段
  * 和纹理片段.
  *
  * @param label 视图id
  * @param candidate_id patch candidate id
  * @param texture_view 视图
  * @param subgraph_faces 此联通子图中的三角面片段
  * @param mesh 整个mesh
  * @param settings 算法配置
  * @return
  */
TexturePatchCandidate
generate_candidate(int label,
                   int candidate_id,
                   TextureView const &texture_view,
                   std::vector<std::size_t> const &subgraph_faces,
                   mve::TriangleMesh::ConstPtr mesh,
                   Settings const &settings) {

  mve::ByteImage::Ptr view_image = texture_view.get_image();
  int min_x = view_image->width(), min_y = view_image->height();
  int max_x = 0, max_y = 0;

  mve::TriangleMesh::FaceList const &mesh_faces = mesh->get_faces();
  mve::TriangleMesh::VertexList const &vertices = mesh->get_vertices();
  //将此联通子图对应的三角面片段投影到视图中，计算整个bbox，计算顶点的纹理坐标
  std::vector<math::Vec2f> texcoords;
  for (std::size_t i = 0; i < subgraph_faces.size(); ++i) {
    for (std::size_t j = 0; j < 3; ++j) {
      math::Vec3f vertex = vertices[mesh_faces[subgraph_faces[i] * 3 + j]];
      math::Vec2f pixel = texture_view.get_pixel_coords(vertex);
      //纹理坐标来自三角形顶点的投影坐标,同一视图下,同一个顶点只有一个投影坐标,
      //因此这里即使两个相邻patch(相邻但标签不同)边界上的顶点，也不会产生两个纹理坐标
      texcoords.push_back(pixel);
      //记录所有子图的并集在视图中的bbox
      min_x = std::min(static_cast<int>(std::floor(pixel[0])), min_x);
      min_y = std::min(static_cast<int>(std::floor(pixel[1])), min_y);
      max_x = std::max(static_cast<int>(std::ceil(pixel[0])), max_x);
      max_y = std::max(static_cast<int>(std::ceil(pixel[1])), max_y);
    }
  }

  /* Check for valid projections/erroneous labeling files.
   * 通常若是单纯投影，则有可能出现bbox超出视图,但由于这里的patch都是有标签的,意味着
   * 在MRF代价表中是代价最低的，那时已做过可见性测试，不可能超出视图;否则就是标签有误*/
  assert(min_x >= 0);
  assert(min_y >= 0);
  assert(max_x < view_image->width());
  assert(max_y < view_image->height());
  //todo:这里好像值得商榷https://github.com/nmoehrle/mvs-texturing/issues/62
  //应该不需要加1，因为区间是[min_x,max_x)
  int width = max_x - min_x + 1;
  int height = max_y - min_y + 1;

  /* Add border and adjust min accordingly.
   * 给bbox加边框,相当于minx和maxx都扩大1像素*/
  width += 2 * texture_patch_border;
  height += 2 * texture_patch_border;
  min_x -= texture_patch_border;
  min_y -= texture_patch_border;

  /* Calculate the relative texcoords.调整之前的纹理坐标,
   * 计算相对纹理坐标:相对于bbox的左上角*/
  math::Vec2f min(min_x, min_y);
  for (std::size_t i = 0; i < texcoords.size(); ++i) {
    texcoords[i] = texcoords[i] - min;
  }
  // todo:从视图中抠出bbox区域的数据并转为三通道浮点图,这几步逐个像素逐个通道地遍历,可改进
  mve::ByteImage::Ptr byte_image;
  byte_image = mve::image::crop(view_image, width, height, min_x, min_y,
                                *math::Vec3uc(255, 0, 255)/*紫色*/);
  mve::FloatImage::Ptr image = mve::image::byte_to_float_image(byte_image);

  if (settings.tone_mapping != TONE_MAPPING_NONE) {
    mve::image::gamma_correct(image, 2.2f);
  }

  TexturePatchCandidate texture_patch_candidate = {
      candidate_id,
      //Rect对象中约定max_x和max_y取不到
      Rect<int>(min_x, min_y, max_x, max_y),
      TexturePatch::create(label, subgraph_faces, texcoords, image)};
#ifdef PATCH_STAT
  std::cout << "Label#"<<label<<"PatchCandidate#"<<candidate_id<<",rect["
  <<min_x<<","<<min_y<<";"<<max_x<<","<<max_y<<")"<<std::endl;
#endif
  return texture_patch_candidate;
}

/**
 * 给未赋予标签值的patch填洞.
 * 当前这个函数只会对满足以下条件的HolePatch补洞：
 * 0、顶点总数不超过100
 * 1、落在mesh内部、标签值=0、不包含任何mesh边缘顶点
 * 2、disk like
 *
 * @param hole_subgraph_faces 洞，即未分配到标签值的三角面联通子图
 * @param graph 整个FFAdj邻接图
 * @param mesh mesh
 * @param mesh_info mesh的顶点拓扑信息
 * @param vertex_projection_infos 当前顶点投影信息结构，如果补洞成功则会被更新
 * @param texture_patches 当前所有纹理patch的集合，如果补洞成功则会被更新
 * @return
 */
bool fill_hole(
    std::vector<std::size_t> const &hole_subgraph_faces,
    UniGraph const &graph,
    mve::TriangleMesh::ConstPtr mesh,
    mve::MeshInfo const &mesh_info,
    std::vector<std::vector<VertexProjectionInfo>> *vertex_projection_infos,
    std::vector<TexturePatch::Ptr> *texture_patches) {

  mve::TriangleMesh::FaceList const &mesh_faces = mesh->get_faces();
  mve::TriangleMesh::VertexList const &vertices = mesh->get_vertices();

  //记录这个联通子图中每个顶点与哪些面邻接(仅限子图中的三角面,子图之外的邻接面不计入tmp)
  std::map<std::size_t, std::set<std::size_t>> tmp;
  for (std::size_t const face_id : hole_subgraph_faces) {
    std::size_t const v0 = mesh_faces[face_id * 3 + 0];
    std::size_t const v1 = mesh_faces[face_id * 3 + 1];
    std::size_t const v2 = mesh_faces[face_id * 3 + 2];
    /* Check topology in original mesh. */
    if (mesh_info[v0].vclass != mve::MeshInfo::VERTEX_CLASS_SIMPLE
    ||mesh_info[v1].vclass != mve::MeshInfo::VERTEX_CLASS_SIMPLE
    ||mesh_info[v2].vclass != mve::MeshInfo::VERTEX_CLASS_SIMPLE) {
      std::cerr<<"error:containing non-simplex,give it up."
      <<"v0cls="<<mesh_info[v0].vclass<<",v1cls="
      <<mesh_info[v1].vclass<<",v2cls="<<mesh_info[v2].vclass<<std::endl;
      return false;
    }
    tmp[v0].insert(face_id);
    tmp[v1].insert(face_id);
    tmp[v2].insert(face_id);
  }

  std::size_t const num_vertices = tmp.size();
  /* Only fill small holes. 只填补不超过100个顶点构成的洞，那三角面的数量呢?*/
  if (num_vertices > MAX_HOLE_NUM_FACES)
    return false;

  /* Calculate 2D parameterization using the technique from libremesh/patch2d,
   * which was published as source code accompanying the following paper:
   *
   * Isotropic Surface Remeshing
   * Simon Fuhrmann, Jens Ackermann, Thomas Kalbe, Michael Goesele
   * 代码来自libremesh中的patch2d.h/patch2d.cc
   */
  std::size_t seed = -1;
  /*NOTE:以下涉及到[局部顶点编号]全都是指patch中顶点的编号,从0开始,按照tmp字典遍历的顺序编排*/
  //记录patch中哪些顶点是patch边缘顶点
  std::vector<bool> is_border(num_vertices, false);
  //记录patch border上的顶点沿着border上的前后相邻的顶点,不在border上的顶点在这个容器中为空
  std::vector<std::vector<std::size_t>> adj_verts_via_border(num_vertices);
  /* map from global vertex id to local index.全局vid->patch内顶点索引 */
  std::map<std::size_t, std::size_t> g2l;
  /*以tmp字典顺序记录patch内每个顶点的全局vid*/
  std::vector<std::size_t> l2g(num_vertices);
  /*记录patch中所有边界顶点的编号和内部顶点的编号，二者分开单独从0开始编号.用在后面构造Ax=b*/
  std::vector<std::size_t> idx(num_vertices);

  std::size_t num_border_vertices = 0;

  //=>遍历tmp字典,检查gaps,找出这个patch的边缘顶点.
  bool disk_topology = true;
  std::map<std::size_t, std::set<std::size_t>>::iterator it = tmp.begin();
  for (std::size_t j = 0; j < num_vertices; ++j, ++it) {
    std::size_t vertex_id = it->first;//此顶点的全局vid
    g2l[vertex_id] = j;//j是此顶点在patch中的局部编号,以字典tmp的遍历顺序递增
    l2g[j] = vertex_id;

    /* Check topology in original mesh.这部分可以去掉,已经移到前面去了.*/
    if (mesh_info[vertex_id].vclass != mve::MeshInfo::VERTEX_CLASS_SIMPLE) {
      /* Complex/Border vertex in original mesh.
       * 若patch包含mesh边界点、孤立点或非流形点,直接返回,因为下面逻辑没有考虑这种顶点.
       */
      disk_topology = false;
      break;
    }

    /* Check new topology and determine if vertex is now at the border. */
    std::vector<std::size_t> const &adj_faces = mesh_info[vertex_id].faces;
    std::set<std::size_t> const &adj_hole_faces = it->second;
    std::vector<std::pair<std::size_t, std::size_t>> fan;
    for (std::size_t k = 0; k < adj_faces.size(); ++k) {
      //逆时针遍历当前顶点的邻接面,这些邻接面构成一个disk
      std::size_t adj_face = adj_faces[k];
      if (graph.get_label(adj_faces[k]) == 0 &&
          adj_hole_faces.find(adj_face) != adj_hole_faces.end()) {
        //fan逆时针记录了当前邻接面的fid，及它在disk中下一个三角面的fid(不一定在adj_hole_faces中)
        std::size_t curr = adj_faces[k];
        std::size_t next = adj_faces[(k + 1) % adj_faces.size()];
        std::pair<std::size_t, std::size_t> pair(curr, next);
        fan.push_back(pair);
      }
    }

    //逆时针遍历fan，统计gap的个数(genus),若存在至少一个gap，
    // 则用adj_verts_via_border记录下patch边缘的顶点
    std::size_t gaps = 0;
    for (std::size_t k = 0; k < fan.size(); k++) {
      std::size_t curr = fan[k].first;
      //取fan中下一个元素，看它的fid
      std::size_t next = fan[(k + 1) % fan.size()].first;
      //判断fan[k]在disk中的后继三角面 是否为 fan[k]在fan中的后继三角面
      if (fan[k].second != next) {
        ++gaps;
        for (std::size_t l = 0; l < 3; ++l) {
          if (mesh_faces[curr * 3 + l] == vertex_id) {
            std::size_t second = mesh_faces[curr * 3 + (l + 2) % 3];
            adj_verts_via_border[j].push_back(second);
          }
          if (mesh_faces[next * 3 + l] == vertex_id) {
            std::size_t first = mesh_faces[next * 3 + (l + 1) % 3];
            adj_verts_via_border[j].push_back(first);
          }
        }
      }
    }//end for k<fan.size()
    //只有一个gap才能把当前顶点看作是patch边界上的顶点，否则就认为patch太复杂退出.
    is_border[j] = gaps == 1;
    /* Check if vertex is now complex.*/
    if (gaps > 1) {//gaps为0的点一定是patch内部的顶点.
      /* Complex vertex in hole.两个gap的应该不太可能出现:
       *               ..
                      ....
                    .. .  ..
                  ..   .   .
                ..     .    ..
               .       .      ..
             ..        .       ..
            ..         .  ●   ....
            ..         .     .    .
           .. ...      .    .      .
          ..    ...    .   .        ..
          .       ...  . ..     ●     .
         ..         .....             ..
         ..............P.................
         .           .....              ..
         .         ... . ....            .
         .  ●     ..   .    ...     ●    ..
         .      ...    .      ...         .
         .     ..      .        ...       ..
         .  ...        .          ...      .
         . ..          .            ...    ..
         ...           .               ...  .
         ....................................
       *  */
      disk_topology = false;
      break;
    }
    //至此只有：gap为1的patch border顶点、patch内部顶点，两类顶点类型都为SIMPLE
    //因此这里的border,应该指的是此patch的分割线,它一定是闭合的,分割线周围一定有另外一个patch
    if (is_border[j]) {
      //记录这个patch边界顶点是patch中的第几个边界点(从0开始编号)
      idx[j] = num_border_vertices++;
      seed = vertex_id;
    } else {
      //记录这个patch内部顶点是patch中的第几个内部顶点(从0开始编号)
      idx[j] = j - num_border_vertices;
    }
  }//end 迭代完毕所有顶点
  tmp.clear();

  /* No disk or genus zero topology:patch中存在非流行成分或者整个patch没有边界*/
  if (!disk_topology || num_border_vertices == 0){
    std::cout<<"warn:non-manifold vtx or no border vtx,skip."<<std::endl;
    return false;
  }

  //=>整理patch border:按特定顺序串联起来
  std::vector<std::size_t> border;
  border.reserve(num_border_vertices);
  //从patch边界的某个种子顶点出发去搜索adj_vertx_via_border.我觉得还不如把adj_vertex_via_border改成字典结构，在这里直接遍历
  std::size_t prev = seed;
  std::size_t curr = seed;
  while (prev == seed || curr != seed) {
    std::size_t next = std::numeric_limits<std::size_t>::max();
    //当前顶点沿着边界上前后相邻的两个点
    std::vector<std::size_t> const &adj_verts = adj_verts_via_border[g2l[curr]];
    for (std::size_t adj_vert : adj_verts) {
      assert(is_border[g2l[adj_vert]]);
      if (adj_vert != prev && adj_vert != curr) {
        next = adj_vert;
        break;
      }
    }
    if (next != std::numeric_limits<std::size_t>::max()) {
      prev = curr;
      curr = next;
      border.push_back(next);
    } else {
      /* No new border vertex */
      border.clear();
      break;
    }

    /* Loop within border */
    if (border.size() > num_border_vertices)
      break;
  }//end while

  if (border.size() != num_border_vertices){
    //走到这里说明前面肯定出错了!
    std::cerr<<"error:border size invalid!"<<std::endl;
    //exit(3);
    return false;
  }

  //=遍历所有边界上的顶点,计算边界总长度、以及它投影到视图上的总长度
  float total_length = 0.0f;
  float total_projection_length = 0.0f;
  for (std::size_t j = 0; j < border.size(); ++j) {
    std::size_t vi0 = border[j];
    std::size_t vi1 = border[(j + 1) % border.size()];
    std::vector<VertexProjectionInfo> vpi0, vpi1;
//#pragma omp critical(vpis)
    {
      vpi0 = vertex_projection_infos->at(vi0);
      vpi1 = vertex_projection_infos->at(vi1);
    }

    /* According to the previous checks (vertex class within the origial
     * mesh and boundary) there has to be at least one projection
     * of each border vertex in a common texture patch. 
     * 由于Hole Patch在任何视图中都不可见,但它的边缘边在某些已分配标签的patch中可见，
     * 这里就是为Hole Patch的每个边缘边，从相邻patch中[偷]投影坐标，其实不见得妥当，因为
     * 不同边缘顶点在不同patch中投影坐标不同,可能会导致一个边缘点的左右两个边来自不同相邻patch.
     */
    math::Vec2f vp0(NAN), vp1(NAN);
    for (VertexProjectionInfo const &info0 : vpi0) {
      for (VertexProjectionInfo const &info1 : vpi1) {
        if (info0.texture_patch_id == info1.texture_patch_id) {
          vp0 = info0.projection;
          vp1 = info1.projection;
          break;
        }
      }
    }
    assert(!std::isnan(vp0[0]) && !std::isnan(vp0[1]));
    assert(!std::isnan(vp1[0]) && !std::isnan(vp1[1]));

    total_projection_length += (vp0 - vp1).norm();
    math::Vec3f const &v0 = vertices[vi0];
    math::Vec3f const &v1 = vertices[vi1];
    total_length += (v0 - v1).norm();
  }//end for j<border.size()

  float radius = total_projection_length / (2.0f * MATH_PI);
  if (total_length < std::numeric_limits<float>::epsilon())
    return false;
  //保存patch所有顶点的参数化坐标
  std::vector<math::Vec2f> projections(num_vertices);
  {//先计算边界上顶点的参数化坐标:将它们按边长比例分配到平面单位圆周上.
    float length = 0.0f;
    for (std::size_t j = 0; j < border.size(); ++j) {
      float angle = 2.0f * MATH_PI * (length / total_length);
      projections[g2l[border[j]]] =
          math::Vec2f(std::cos(angle), std::sin(angle));
      math::Vec3f const &v0 = vertices[border[j]];
      math::Vec3f const &v1 = vertices[border[(j + 1) % border.size()]];
      length += (v0 - v1).norm();
    }
  }
  //然后对patch内部顶点 进行参数化
  typedef Eigen::Triplet<float, int> Triplet;
  std::vector<Triplet> coeff;
  std::size_t matrix_size = num_vertices - border.size();

  Eigen::VectorXf xx(matrix_size), xy(matrix_size);

  if (matrix_size != 0) {
    Eigen::VectorXf bx(matrix_size);
    Eigen::VectorXf by(matrix_size);
    for (std::size_t j = 0; j < num_vertices; ++j) {
      if (is_border[j])
        continue;//patch边缘顶点并不参加MVC方程计算

      std::size_t const vertex_id = l2g[j];//当前顶点全局编号

      /* Calculate "Mean Value Coordinates" as proposed by Michael S. Floater */
      std::map<std::size_t, float> weights;//记录当前顶点的邻接边构成的权重
      //遍历当前顶点的邻接面列表，由于是patch内部点，其邻接面列表可以直接从mesh_info中取
      std::vector<std::size_t> const &adj_faces = mesh_info[vertex_id].faces;
      for (std::size_t adj_face : adj_faces) {
        std::size_t v0 = mesh_faces[adj_face * 3 + 0];
        std::size_t v1 = mesh_faces[adj_face * 3 + 1];
        std::size_t v2 = mesh_faces[adj_face * 3 + 2];
        if (v1 == vertex_id)//将v0记为当前顶点,此时逆时针顺序已经无法确保
          std::swap(v1, v0);
        if (v2 == vertex_id)
          std::swap(v2, v0);

        math::Vec3f v01 = vertices[v1] - vertices[v0];
        float v01n = v01.norm();
        math::Vec3f v02 = vertices[v2] - vertices[v0];
        float v02n = v02.norm();

        /* Ensure numerical stability */
        if (v01n * v02n < std::numeric_limits<float>::epsilon()){
          std::cout<<"error:vtx("<<vertices[v0][0]<<","
          <<vertices[v0][1]<<","<<vertices[v0][2]<<") has too short edge!quit."<<std::endl;
          return false;
        }
          
        float calpha = v01.dot(v02) / (v01n * v02n);
        float alpha = std::acos(clamp(calpha, -1.0f, 1.0f));
        //计算技巧:此处将当前邻接面的v0相关角alpha一分为二，各自正切值除以v01和v02长度一半，
        //当前顶点v0的所有邻接面遍历一遍之后，累加完毕所得的weights字典就满足MVC
        weights[g2l[v1]] += std::tan(alpha / 2.0f) / (v01n / 2.0f);
        weights[g2l[v2]] += std::tan(alpha / 2.0f) / (v02n / 2.0f);
      }//end for adj_faces

      std::map<std::size_t, float>::iterator it;
      float sum = 0.0f;//这是记录所有的权重之和,因为它在计算标准化权重lambda时作为分母
      for (it = weights.begin(); it != weights.end(); ++it)
        sum += it->second;
      if (sum < std::numeric_limits<float>::epsilon()){
        std::cout<<"error:weights sum too small! quit."<<std::endl;
        return false;
      }
      //对当前顶点的所有权重进行单位化
      for (it = weights.begin(); it != weights.end(); ++it)
        it->second /= sum;
      //在矩阵A和向量b中，填充当前节点j(patch内部点)的成分.A的宽高和b的高都是patch内部点总数.
      bx[idx[j]] = 0.0f;
      by[idx[j]] = 0.0f;
      for (it = weights.begin(); it != weights.end(); ++it) {
        //遍历内部点j的所有邻接点/边的权重，若邻接点是内部点则贡献到A，否则贡献到向量b.
        if (is_border[it->first]) {
          //idx[it->first]记录的是这个边缘顶点是patch中第几个边缘顶点,
          //border数组保存边缘顶点的顺序刚好就是沿着tmp字典的遍历顺序逐个记录边缘顶点
          std::size_t border_vertex_id = border[idx[it->first]];
          math::Vec2f projection = projections[g2l[border_vertex_id]];
          bx[idx[j]] += projection[0] * it->second;
          by[idx[j]] += projection[1] * it->second;
        } else {
          //j和邻接点it->first都是patch内部点，贡献到A矩阵
          coeff.push_back(Triplet(idx[j], idx[it->first], -it->second));
        }
      }
    }//end for j<num_vertices
    //对角线上都是1.f
    for (std::size_t j = 0; j < matrix_size; ++j) {
      coeff.push_back(Triplet(j, j, 1.0f));
    }
    //A矩阵不是对称阵,只是方阵可用LU分解
    typedef Eigen::SparseMatrix<float> SpMat;
    SpMat A(matrix_size, matrix_size);
    //coeff是Coordinate Format格式的稀疏矩阵表示法:每个nnz都用(i,j,val)表示
    A.setFromTriplets(coeff.begin(), coeff.end());
    A.makeCompressed();//Eigen::SparseLU建议A是压缩且列主序存储
    Eigen::SparseLU<SpMat> solver;
    solver.analyzePattern(A);
    solver.factorize(A);
    xx = solver.solve(bx);
    xy = solver.solve(by);
  }

  float const max_hole_patch_size = MAX_HOLE_PATCH_SIZE;
  //确定此HolePatch投影区域大小，最大不超过100x100
  int image_size =
      std::min(std::floor(radius * 1.1f) * 2.0f, max_hole_patch_size);
  /* Ensure a minimum scale of one */
  image_size += 2 * (1 + texture_patch_border);
  int scale = image_size / 2 - texture_patch_border;//半径方向的扩展倍数,图像宽高的一半
  //将参数化的单位圆,按scale倍数扩展,投影坐标的原点顺便移到左上角
  for (std::size_t j = 0, k = 0; j < num_vertices; ++j) {
    if (is_border[j]) {
      projections[j] = projections[j] * scale + image_size / 2;
    } else {
      projections[j] = math::Vec2f(xx[k], xy[k]) * scale + image_size / 2;
      ++k;
    }
  }
  //记录此Hole Patch每个顶点的纹理坐标
  std::vector<math::Vec2f> texcoords;
  texcoords.reserve(hole_subgraph_faces.size());
  for (std::size_t const face_id : hole_subgraph_faces) {
    for (std::size_t j = 0; j < 3; ++j) {
      std::size_t const vertex_id = mesh_faces[face_id * 3 + j];
      math::Vec2f const &projection = projections[g2l[vertex_id]];
      texcoords.push_back(projection);
    }
  }
  //为此Hole Patch创建TexturePatch对象，加入到全局patch列表
  mve::FloatImage::Ptr image = mve::FloatImage::create(image_size, 
            image_size, 3);//image_size包含了texture_border
  // DEBUG image->fill_color(*math::Vec4uc(0, 255, 0, 255));
  TexturePatch::Ptr texture_patch =
      TexturePatch::create(0, hole_subgraph_faces, texcoords, image);
  std::size_t texture_patch_id;
//#pragma omp critical
  {
    texture_patches->push_back(texture_patch);
    texture_patch_id = texture_patches->size() - 1;
  }
  //为此Hole Patch内每个顶点计算VPI,并添加到全局VPI列表
  for (std::size_t j = 0; j < num_vertices; ++j) {
    std::size_t const vertex_id = l2g[j];
    std::vector<std::size_t> const &adj_faces = mesh_info[vertex_id].faces;
    std::vector<std::size_t> faces;
    faces.reserve(adj_faces.size());
    for (std::size_t adj_face : adj_faces) {
      if (graph.get_label(adj_face) == 0) {
        faces.push_back(adj_face);
      }
    }
    VertexProjectionInfo info = {texture_patch_id, projections[j], faces};
//#pragma omp critical(vpis)
    vertex_projection_infos->at(vertex_id).push_back(info);
  }

  return true;
}//end fill_hole

/**
 * 根据UniGraph的标签分配方案，为mesh生成若干TexturePatch.
 * @param [out] graph 整个FFAdj拓扑图,也记录了每个三角面分配到的标签值.在本函数中也会为
 * 每个三角面记录其所属的texture patch id.
 * @param mesh 三角网格
 * @param mesh_info 顶点拓扑信息
 * @param texture_views 所有的视图
 * @param settings 算法配置
 * @param vertex_projection_infos 整个mesh中每个顶点的投影信息，其结构如下:
 * @param texture_patches 全局patch列表,记录了所有视图中提取的所有patch.
 */
void generate_texture_patches(
    UniGraph &graph,
    mve::TriangleMesh::ConstPtr mesh,
    mve::MeshInfo const &mesh_info,
    std::vector<TextureView> *texture_views,
    Settings const &settings,
    std::vector<std::vector<VertexProjectionInfo>> *vertex_projection_infos,
    std::vector<TexturePatch::Ptr> *texture_patches) {

  util::WallTimer timer;

  mve::TriangleMesh::FaceList const &mesh_faces = mesh->get_faces();
  mve::TriangleMesh::VertexList const &vertices = mesh->get_vertices();
  vertex_projection_infos->resize(vertices.size());
  //全局所有视图贡献的patch总数
  std::size_t num_patches = 0;

  std::cout << "\tRunning... " << std::flush;
// todo:需要优化并行度
//#pragma omp parallel for schedule(dynamic)
  for (std::size_t i = 0; i < texture_views->size(); ++i) {//omp-parallel-for
    //当前视图中的所有联通子图，每个子图用一片fid表示
    std::vector<std::vector<std::size_t>> subgraphs;
    //label从1开始，意味着subgraphs中只包含已分配到标签的子图
    int const label = i + 1;
    // todo:这里在整个mesh中找出所有标签值为label的联通子图,外面还有一层循环,是否可加速
    graph.get_subgraphs(label, &subgraphs);

    TextureView *texture_view = &texture_views->at(i);
    texture_view->load_image(); //读取视图图片
    //为当前视图中的每个子图，提取图像片段，生成相对纹理坐标
    std::list<TexturePatchCandidate> candidates;
    for (std::size_t j = 0; j < subgraphs.size(); ++j) {
      candidates.push_back(generate_candidate(label,j, *texture_view,
                                              subgraphs[j], mesh, settings));
    }
    texture_view->release_image();

    /* Merge candidates which contain the same image content.
     * 合并任意两个存在包含关系的candidate,合二为一,避免patch并排放置占空间 */
    std::list<TexturePatchCandidate>::iterator it, sit;
    for (it = candidates.begin(); it != candidates.end(); ++it) {
      for (sit = candidates.begin(); sit != candidates.end();) {
        Rect<int> bounding_box = sit->bounding_box;
        //可合并条件：两个patch不同、一个bbox包含另一个bbox
        if (it != sit && bounding_box.is_inside(&it->bounding_box)) {
          #ifdef PATCH_STAT
          std::cerr << "info:L"<<it->texture_patch->get_label()<<"pc#"
                    <<it->cand_id<<" merge L"<<sit->texture_patch->get_label()
                    << "pc#" << sit->cand_id <<std::endl;
          #endif
          //合并两个独立的联通子图
          TexturePatch::Faces &faces = it->texture_patch->get_faces();
          TexturePatch::Faces &ofaces = sit->texture_patch->get_faces();
          faces.insert(faces.end(), ofaces.begin(), ofaces.end());
          //合并两个相对纹理坐标集合
          TexturePatch::Texcoords &texcoords =
              it->texture_patch->get_texcoords();
          TexturePatch::Texcoords &otexcoords =
              sit->texture_patch->get_texcoords();
          math::Vec2f offset;
          offset[0] = sit->bounding_box.min_x - it->bounding_box.min_x;
          offset[1] = sit->bounding_box.min_y - it->bounding_box.min_y;
          for (std::size_t i = 0; i < otexcoords.size(); ++i) {
            texcoords.push_back(otexcoords[i] + offset);
          }
          //移除较小的内部patch candidate
          sit = candidates.erase(sit);
        } else {
          ++sit;
        }
      }
    } // end for it!=candidates.end()

    //至此已分配到标签的candidate已合并完毕,为这些patch计算VPI
    it = candidates.begin();
    for (; it != candidates.end(); ++it) {
      std::size_t texture_patch_id;
//#pragma omp critical
      { //将当前视图中的patch全部追加到全局patch列表
        texture_patches->push_back(it->texture_patch);
        texture_patch_id = num_patches++;//从0开始
      }
      //遍历此patch中的所有三角面的顶点，计算每个顶点投影信息(VPI),加入到全局VPI列表
      std::vector<std::size_t> const &faces = it->texture_patch->get_faces();
      std::vector<math::Vec2f> const &texcoords =
          it->texture_patch->get_texcoords();
      for (std::size_t i = 0; i < faces.size(); ++i) {
        std::size_t const face_id = faces[i];
        std::size_t const face_pos = face_id * 3;
        for (std::size_t j = 0; j < 3; ++j) {
          std::size_t const vertex_id = mesh_faces[face_pos + j];
          //这是相对于patch图片左上角的坐标，也是相对纹理坐标
          math::Vec2f const projection = texcoords[i * 3 + j];
          /*
           * 构造此顶点在此patch中的face_id三角面上的投影信息.
           * 综合来看,vertex_projection_infos的结构会类似于下面这样：
           *        +--------------------+
                    |VPI:0,(u0,v0),{F0}  +-+
            +--+    +--------------------+ |相同patch意味着相同纹理坐标
            |V0+----+VPI:0,(u0,v0),{F1}  +-+
            |  |    +--------------------+ |不同patch,纹理坐标相同,可能来自相同视图
            |V1|    |VPI:1,(u0,v0),{F3}  +-+
            |  | .  +--------------------+ |不同patch,不同纹理坐标,来自不同视图
            |  | .  |VPI:3,(u1,v1),{F6}  +-+
            |  | .  +--------------------+
            |..| .
            |  |
            |  |
            |Vn|
            +--+
           */
          VertexProjectionInfo info = {texture_patch_id, projection, {face_id}};

//#pragma omp critical
          vertex_projection_infos->at(vertex_id).push_back(info);
        } // end for j<3 迭代三个顶点
      }// end for i<faces.size() 迭代patch候选中子图的所有三角面
    }// end for it!=candidates.end() 迭代某视图的所有patch候选
  }// end for i < texture_views.size() 迭代所有视图

  merge_vertex_projection_infos(vertex_projection_infos);

  /* == Handle-Hole-Patch == */
  {//找没有分配到标签的三角面 -- Holes
    std::vector<std::size_t> unseen_faces;
    std::vector<std::vector<std::size_t>> subgraphs;
    graph.get_subgraphs(0, &subgraphs);

//#pragma omp parallel for schedule(dynamic)
    for (std::size_t i = 0; i < subgraphs.size(); ++i) {//omp-parallel-for
      std::vector<std::size_t> const &subgraph = subgraphs[i];

      bool success = false;
      if (settings.hole_filling) {
        success = fill_hole(subgraph, graph, mesh, mesh_info,
                            vertex_projection_infos, texture_patches);
      }
      //已经补洞了的hole patch不算作unseen faces
      if (success) {
        num_patches += 1;
      } else {
        if (settings.keep_unseen_faces) {
//#pragma omp critical
          unseen_faces.insert(unseen_faces.end(), subgraph.begin(),
                              subgraph.end());
        }
      }
    }

    if (!unseen_faces.empty()) {//所有unseen faces作为一个patch加入全局patch列表
      mve::FloatImage::Ptr image = mve::FloatImage::create(3, 3, 3);
      std::vector<math::Vec2f> texcoords;
      for (std::size_t i = 0; i < unseen_faces.size(); ++i) {
        math::Vec2f projections[] = {{2.0f, 1.0f}, {1.0f, 1.0f}, {1.0f, 2.0f}};
        texcoords.insert(texcoords.end(), &projections[0], &projections[3]);
      }
      TexturePatch::Ptr texture_patch =
          TexturePatch::create(0, unseen_faces, texcoords, image);
      texture_patches->push_back(texture_patch);
      std::size_t texture_patch_id = texture_patches->size() - 1;

      for (std::size_t i = 0; i < unseen_faces.size(); ++i) {
        std::size_t const face_id = unseen_faces[i];
        std::size_t const face_pos = face_id * 3;
        for (std::size_t j = 0; j < 3; ++j) {
          std::size_t const vertex_id = mesh_faces[face_pos + j];
          math::Vec2f const projection = texcoords[i * 3 + j];

          VertexProjectionInfo info = {texture_patch_id, projection, {face_id}};

          vertex_projection_infos->at(vertex_id).push_back(info);
        }
      }
    }
  }//end of Handle-Hole-Patch

  //再次整理全局VPI，合并(todo:若前面HolePatch没有贡献任何新的VPI时，这里可以跳过)
  merge_vertex_projection_infos(vertex_projection_infos);

  //cl-add:并行遍历所有patch(包括hole patch也要),在UniGraph中记住每个面的patchid
  auto &f2p=graph.get_face2patchid();
  f2p.resize(mesh_faces.size()/3);
  //#pragma omp parallel for schedule(dynamic)
  for(auto i=0; i<texture_patches->size(); i++){
    TexturePatch::Ptr &patch=texture_patches->at(i);
    const auto &faces=patch->get_faces();
    //这里可并行,但应该没必要
    for(auto j=0;j<faces.size(); j++){
      f2p[faces[j]]=i;
    }
  }
  std::cout << "done. (Took " << timer.get_elapsed_sec() << "s)" << std::endl;
  std::cout << "\t" << num_patches << " texture patches." << std::endl;
}

TEX_NAMESPACE_END

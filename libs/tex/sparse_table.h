/*
 * Copyright (C) 2015, Nils Moehrle
 * TU Darmstadt - Graphics, Capture and Massively Parallel Computing
 * All rights reserved.
 *
 * This software may be modified and distributed under the terms
 * of the BSD 3-Clause license. See the LICENSE.txt file for details.
 */

#ifndef TEX_SPARSETABLE_HEADER
#define TEX_SPARSETABLE_HEADER

#include <vector>

#include <cerrno>
#include <cstring>
#include <fstream>
#include <iostream>

#include "util/exception.h"
#include "util/file_system.h"


#define HEADER "SPT"
#define VERSION "0.2"

TEX_NAMESPACE_BEGIN
 /**
  * Class representing a sparse table optimized for row and column wise access.
  * 稀疏表.
  * 为按行和按列访问做了优化, 可以以行号取出这一行的所有列, 也可以通过列号取出这一列的所有行.
  * 为MRF组合问题提供了数据访问上的便利,假如列总数等于mesh三角面总数，行总数代表视图总数，
  * 经常需要查询某个三角面在哪些视图下可见，也需要查询某个视图下哪些三角形可见.
  *
  * TODO:保持语义不变，将SparseTable改为支持并行操作的数据结构
  *
  * @tparam C 列编号的类型,如uint32_t
  * @tparam R 行编号的类型,如uint16_t
  * @tparam T 元素类型,如float
  */
template <typename C, typename R, typename T>
class SparseTable {
public:

  ///<稀疏表中的列(稀疏的).代表某一列数据构成的数组，数组每一项数据用[行号,元素值]表示
  typedef std::vector<std::pair<R, T>> Column;

  /**
   * 稀疏表中的行(稀疏的).代表某一行数据构成的数组，数组每一项数据用[列号,元素值]表示
   *
   * @warning 由于行视图下的子列表和列视图下的子列表规模可能不对等，可能会使得部分查询操作比
    * 较低效，比如上面的例子中，行视图下，每一行下的所有元素代表了该视图下可见的所有三角面，
    * 这个数目可能通常会比较大，若要查询某个三角面f在视图i中是否可见，则如果每一行使用简单
    * 的数组查询起来比较低效.
   */
  typedef std::vector<std::pair<C, T>> Row;

private:
  //按列组织的全部数据
  std::vector<Column> column_wise_data;
  //按行组织的全部数据
  std::vector<Row> row_wise_data;
  /**
   * number of nonzero matrix elements，matlab中同名函数.
   * 稀疏表中真正有值的元素，有的值也是0.
   */
  std::size_t nnz;

public:
  SparseTable();
  SparseTable(C cols, R rows);

  C cols() const;
  R rows() const;

  Column const &col(C id) const;
  Row const &row(R id) const;

  void set_value(C col, R row, T value);

  std::size_t get_nnz() const;

  /**
   * Saves the SparseTable to the file given by filename.
   * Version and size information is stored in ascii, values in binary.
   * @throws util::FileException
   */
  static void save_to_file(SparseTable const &sparse_table,
                           std::string const &filename);

  /**
   * Loads a SparseTable from the file given by filename.
   * @throws util::FileException if the file does not exist or if the header
   * does not matches.
   */
  static void load_from_file(std::string const &filename,
                             SparseTable *sparse_table);
};

template <typename C, typename R, typename T>
std::size_t SparseTable<C, R, T>::get_nnz() const {
  return nnz;
}

template <typename C, typename R, typename T>
C SparseTable<C, R, T>::cols() const {
  return column_wise_data.size();
}

template <typename C, typename R, typename T>
R SparseTable<C, R, T>::rows() const {
  return row_wise_data.size();
}

template <typename C, typename R, typename T>
typename SparseTable<C, R, T>::Column const &
SparseTable<C, R, T>::col(C id) const {
  return column_wise_data[id];
}

template <typename C, typename R, typename T>
typename SparseTable<C, R, T>::Row const &
SparseTable<C, R, T>::row(R id) const {
  return row_wise_data[id];
}

template <typename C, typename R, typename T>
SparseTable<C, R, T>::SparseTable() {
  nnz = 0;
}

template <typename C, typename R, typename T>
SparseTable<C, R, T>::SparseTable(C cols, R rows) {
  column_wise_data.resize(cols);
  row_wise_data.resize(rows);
  nnz = 0;
}

template <typename C, typename R, typename T>
void SparseTable<C, R, T>::set_value(C col, R row, T value) {
  column_wise_data[col].push_back(std::pair<R, T>(row, value));
  row_wise_data[row].push_back(std::pair<C, T>(col, value));
  nnz++;
}

template <typename C, typename R, typename T>
void SparseTable<C, R, T>::save_to_file(SparseTable const &sparse_table,
                                        const std::string &filename) {
  std::ofstream out(filename.c_str(), std::ios::binary);
  if (!out.good())
    throw util::FileException(filename, std::strerror(errno));

  C cols = sparse_table.cols();
  R rows = sparse_table.rows();
  std::size_t nnz = sparse_table.get_nnz();
  out << HEADER << " " << VERSION << " " << cols << " " << rows << " " << nnz
      << std::endl;

  for (C col = 0; col < cols; ++col) {
    SparseTable::Column column = sparse_table.col(col);
    for (std::size_t i = 0; i < column.size(); ++i) {
      std::pair<R, T> entry = column[i];
      R row = entry.first;
      T value = entry.second;

      out.write((char const *)&col, sizeof(C));
      out.write((char const *)&row, sizeof(R));
      out.write((char const *)&value, sizeof(T));
    }
  }
  out.close();
}

template <typename C, typename R, typename T>
void SparseTable<C, R, T>::load_from_file(const std::string &filename,
                                          SparseTable<C, R, T> *sparse_table) {
  std::ifstream in(filename.c_str(), std::ios::binary);
  if (!in.good())
    throw util::FileException(filename, std::strerror(errno));

  std::string header;

  in >> header;

  if (header != HEADER) {
    in.close();
    throw util::FileException(filename, "Not a SparseTable file!");
  }

  std::string version;
  in >> version;

  if (version != VERSION) {
    in.close();
    throw util::FileException(filename,
                              "Incompatible version of SparseTable file!");
  }

  C cols;
  R rows;
  std::size_t nnz;
  in >> cols >> rows >> nnz;

  if (cols != sparse_table->cols() || rows != sparse_table->rows()) {
    in.close();
    throw util::FileException(filename, "SparseTable has different dimension!");
  }

  std::string buffer;

  /* Discard the rest of the line. */
  std::getline(in, buffer);

  C col;
  R row;
  T value;
  for (std::size_t i = 0; i < nnz; ++i) {
    in.read((char *)&col, sizeof(C));
    in.read((char *)&row, sizeof(R));
    in.read((char *)&value, sizeof(T));
    sparse_table->set_value(col, row, value);
  }

  in.close();
}

TEX_NAMESPACE_END

#endif /* TEX_SPARSETABLE_HEADER */

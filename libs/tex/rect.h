/*
 * Copyright (C) 2015, Nils Moehrle
 * TU Darmstadt - Graphics, Capture and Massively Parallel Computing
 * All rights reserved.
 *
 * This software may be modified and distributed under the terms
 * of the BSD 3-Clause license. See the LICENSE.txt file for details.
 */

#ifndef TEX_RECT_HEADER
#define TEX_RECT_HEADER

#include <cassert>
#include <sstream>
#include "defines.h"
#include "math/vector.h"

TEX_NAMESPACE_BEGIN
/**
 * Simple class representing a rectangle.
 *
 * [min_x, max_x),
 *
 * [min_y,max_y)
 *
 * 取不到max_x和max_y!!
 */
template <typename T>
class Rect {
public:
  T min_x;
  T min_y;
  T max_x;
  T max_y;

  Rect<T>(void);
  Rect<T>(Rect<T> *rect);
  Rect<T>(T min_x, T min_y, T max_x, T max_y, bool checkAssertion=true);

  T width() const;
  T height() const;
  T size() const;

  /** Returns true if the rectangle is within or on the border of the given
   * rectangle. */
  bool is_inside(Rect const *orect) const;
  /** Returns true if the rectangle intersects with the given rectangle. */
  bool intersect(Rect const *orect) const;

  void update(T min_x, T min_y, T max_x, T max_y);
  /** Moves the rectangle and updates its position. */
  void move(T x, T y);
  template <class P>
  bool contain_point(const math::Vector<P,2> &point){
    return min_x<=point[0] && point[0]<max_x && min_y<=point[1]&&point[1]<max_y;
  }
  std::string to_string()const {
    std::ostringstream oss;
    oss<<"{["<<min_x<<","<<max_x<<");["<<min_y<<","<<max_y<<")}";
    return oss.str();
  }
};

/**
 * 创建一个1x1的Rect
 * @tparam T
 */
template <typename T>
Rect<T>::Rect() {
  update(0, 0, 1, 1);
}

template <typename T>
Rect<T>::Rect(Rect<T> *rect) {
  update(rect->min_x, rect->min_y, rect->max_x, rect->max_y);
}

template <typename T>
Rect<T>::Rect(T min_x, T min_y, T max_x, T max_y, bool checkAssertion) {
  if (checkAssertion){
    update(min_x, min_y, max_x, max_y);
  }else{
    this->min_x=min_x;
    this->min_y=min_y;
    this->max_x=max_x;
    this->max_y=max_y;
  }
}

/**
 *
 * 返回max_x - min_x
 * @tparam T
 * @return
 */
template <typename T>
inline T Rect<T>::width() const {
  return max_x - min_x;
}

/**
 * 返回max_y - min_y
 * @tparam T
 * @return
 */
template <typename T>
inline T Rect<T>::height() const {
  return max_y - min_y;
}

template <typename T>
inline T Rect<T>::size() const {
  return width() * height();
}

template <typename T>
inline bool Rect<T>::is_inside(Rect const *rect) const {
  return min_x >= rect->min_x && max_x <= rect->max_x && min_y >= rect->min_y &&
         max_y <= rect->max_y;
}

template <typename T>
inline void Rect<T>::update(T min_x, T min_y, T max_x, T max_y) {
  this->min_x = min_x;
  this->min_y = min_y;
  this->max_x = max_x;
  this->max_y = max_y;
  assert(min_x <= max_x);
  assert(min_y <= max_y);
}

template <typename T>
inline bool Rect<T>::intersect(Rect<T> const *rect) const {
  return min_x < rect->max_x && max_x > rect->min_x && min_y < rect->max_y &&
         max_y > rect->min_y;
}

template <typename T>
inline void Rect<T>::move(T x, T y) {
  update(x, y, x + width(), y + height());
}
TEX_NAMESPACE_END

#endif /* TEX_RECT_HEADER */

/*
 * Copyright (C) 2015, Nils Moehrle, Michael Waechter
 * TU Darmstadt - Graphics, Capture and Massively Parallel Computing
 * All rights reserved.
 *
 * This software may be modified and distributed under the terms
 * of the BSD 3-Clause license. See the LICENSE.txt file for details.
 */

#include <numeric>

#include <Eigen/Core>
#include <Eigen/LU>
#include <acc/bvh_tree.h>
#include <mve/image_color.h>

#include "histogram.h"
#include "progress_counter.h"
#include "sparse_table.h"
#include "texturing.h"
#include "util.h"

//BVH树，用于做遮挡测试
typedef acc::BVHTree<unsigned int, math::Vec3f> BVHTree;

TEX_NAMESPACE_BEGIN

/**
 * Dampens the quality of all views in which the face's projection
 * has a much different color than in the majority of views.
 * Returns whether the outlier removal was successfull.
 *
 * @param infos contains information about one face seen from several views
 * @param settings runtime configuration.
 */
bool photometric_outlier_detection(std::vector<FaceProjectionInfo> *infos,
                                   Settings const &settings) {
  if (infos->size() == 0)
    return true;

  /* Configuration variables. */

  double const gauss_rejection_threshold = 6e-3;

  /* If all covariances drop below this we stop outlier detection. */
  double const minimal_covariance = 5e-4;

  int const outlier_detection_iterations = 10;
  int const minimal_num_inliers = 4;

  float outlier_removal_factor = std::numeric_limits<float>::signaling_NaN();
  switch (settings.outlier_removal) {
  case OUTLIER_REMOVAL_NONE: return true;
  case OUTLIER_REMOVAL_GAUSS_CLAMPING: outlier_removal_factor = 1.0f; break;
  case OUTLIER_REMOVAL_GAUSS_DAMPING: outlier_removal_factor = 0.2f; break;
  }

  Eigen::MatrixX3d inliers(infos->size(), 3);
  std::vector<std::uint32_t> is_inlier(infos->size(), 1);
  for (std::size_t row = 0; row < infos->size(); ++row) {
    inliers.row(row) = mve_to_eigen(infos->at(row).mean_color).cast<double>();
  }

  Eigen::RowVector3d var_mean;
  Eigen::Matrix3d covariance;
  Eigen::Matrix3d covariance_inv;

  for (int i = 0; i < outlier_detection_iterations; ++i) {

    if (inliers.rows() < minimal_num_inliers) {
      return false;
    }

    /* Calculate the inliers' mean color and color covariance. */
    var_mean = inliers.colwise().mean();
    Eigen::MatrixX3d centered = inliers.rowwise() - var_mean;
    covariance = (centered.adjoint() * centered) / double(inliers.rows() - 1);

    /* If all covariances are very small we stop outlier detection
     * and only keep the inliers (set quality of outliers to zero). */
    if (covariance.array().abs().maxCoeff() < minimal_covariance) {
      for (std::size_t row = 0; row < infos->size(); ++row) {
        if (!is_inlier[row])
          infos->at(row).quality = 0.0f;
      }
      return true;
    }

    /* Invert the covariance. FullPivLU is not the fastest way but
     * it gives feedback about numerical stability during inversion. */
    Eigen::FullPivLU<Eigen::Matrix3d> lu(covariance);
    if (!lu.isInvertible()) {
      return false;
    }
    covariance_inv = lu.inverse();

    /* Compute new number of inliers (all views with a gauss value above a
     * threshold). */
    for (std::size_t row = 0; row < infos->size(); ++row) {
      Eigen::RowVector3d color =
          mve_to_eigen(infos->at(row).mean_color).cast<double>();
      double gauss_value =
          multi_gauss_unnormalized(color, var_mean, covariance_inv);
      is_inlier[row] = (gauss_value >= gauss_rejection_threshold ? 1 : 0);
    }
    /* Resize Eigen matrix accordingly and fill with new inliers. */
    inliers.resize(std::accumulate(is_inlier.begin(), is_inlier.end(), 0),
                   Eigen::NoChange);
    for (std::size_t row = 0, inlier_row = 0; row < infos->size(); ++row) {
      if (is_inlier[row]) {
        inliers.row(inlier_row++) =
            mve_to_eigen(infos->at(row).mean_color).cast<double>();
      }
    }
  }

  covariance_inv *= outlier_removal_factor;
  for (FaceProjectionInfo &info : *infos) {
    Eigen::RowVector3d color = mve_to_eigen(info.mean_color).cast<double>();
    double gauss_value =
        multi_gauss_unnormalized(color, var_mean, covariance_inv);
    assert(0.0 <= gauss_value && gauss_value <= 1.0);
    switch (settings.outlier_removal) {
    case OUTLIER_REMOVAL_NONE: return true;
    case OUTLIER_REMOVAL_GAUSS_DAMPING: info.quality *= gauss_value; break;
    case OUTLIER_REMOVAL_GAUSS_CLAMPING:
      if (gauss_value < gauss_rejection_threshold)
        info.quality = 0.0f;
      break;
    }
  }
  return true;
}

inline bool isPointInFacialFeatureRect(const math::Vec2f &point,
    const FacialFeatureRectPtr &featureRectPtr){
  auto it=featureRectPtr->begin();
  for(;it!=featureRectPtr->end(); it++){
    if (it->contain_point(point)){
      return true;
    }
  }
  return false;
}

inline bool isTriangleInFacialFeatureRect(const math::Vec3f &v1,
                                          const math::Vec3f &v2,
                                          const math::Vec3f &v3,
                                          const TextureView *view,
                                          const FacialFeatureRectPtr &featureRectPtr){
  int count=(int)isPointInFacialFeatureRect(view->get_pixel_coords(v1),featureRectPtr)
  +(int)isPointInFacialFeatureRect(view->get_pixel_coords(v2),featureRectPtr)
  +(int)isPointInFacialFeatureRect(view->get_pixel_coords(v3),featureRectPtr);
  return count>=1;
}

/**
 * 计算每个三角面的投影到不同视图之后的投影信息，填充face_projection_infos.
 * @param mesh 三角网格
 * @param texture_views 所有视图
 * @param settings 算法配置
 * @param[out] face_projection_infos 待填充的投影信息
 * @param frontal_view_id
 * @param featureRectPtr
 * @return max quality
 */
float calculate_face_projection_infos(
    mve::TriangleMesh::ConstPtr mesh,
    std::vector<TextureView> *texture_views,
    Settings const &settings,
    FaceProjectionInfos *face_projection_infos,
    std::size_t frontal_view_id,
    const FacialFeatureRectPtr &featureRectPtr) {

  std::vector<unsigned int> const &faces = mesh->get_faces();
  std::vector<math::Vec3f> const &vertices = mesh->get_vertices();
  mve::TriangleMesh::NormalList const &face_normals = mesh->get_face_normals();

  std::size_t const num_views = texture_views->size();

  util::WallTimer timer;
  std::cout << "\tBuilding BVH from " << faces.size() / 3 << " faces... "
            << std::flush;
  BVHTree bvh_tree(faces, vertices);
  std::cout << "done. (Took: " << timer.get_elapsed() << " ms)" << std::endl;

  ProgressCounter view_counter("\tCalculating face qualities", num_views);
  std::vector<float> max_qualities(num_views,std::numeric_limits<float>::lowest());
//#pragma omp parallel
  {
    //omp parallel制导指令，每个线程都要执行大括号内全部语句.大括号必须在parallel制导指令后面另起一行
    std::vector<std::pair<std::size_t, FaceProjectionInfo>>
        projected_face_view_infos;
    //omp for制导指令用于尽量平均摊派for循环给所有线程，每个线程执行其中一个迭代
//#pragma omp for schedule(dynamic)
    for (std::uint16_t j = 0; j < static_cast<std::uint16_t>(num_views); ++j) {
      ///=>1.迭代每个视图
      view_counter.progress<SIMPLE>();

      TextureView *texture_view = &texture_views->at(j);
      texture_view->load_image();
      //warn:validity mask的目的是什么?
      texture_view->generate_validity_mask();

      if (settings.data_term == DATA_TERM_GMI) {
        //生成梯度图、invalid mask腐蚀valid区域
        texture_view->generate_gradient_magnitude();
        texture_view->erode_validity_mask();
      }
      //光心的世界坐标
      math::Vec3f const &view_pos = texture_view->get_pos();
      //光轴的世界坐标
      math::Vec3f const &viewing_direction =
          texture_view->get_viewing_direction();
      //todo:这里面遍历mesh的每个三角面求投影quality,可以用GPU计算.
      if (j==frontal_view_id){
        std::cout<<"Faces within Facial Feature Rects:"<<std::endl;
      }
      for (std::size_t i = 0; i < faces.size(); i += 3) {
        ///=>2.迭代每个三角面:可见性测试、计算三角面的梯度积分
        std::size_t face_id = i / 3;
        math::Vec3f const &v1 = vertices[faces[i]];
        math::Vec3f const &v2 = vertices[faces[i + 1]];
        math::Vec3f const &v3 = vertices[faces[i + 2]];
        math::Vec3f const &face_normal = face_normals[face_id];
        math::Vec3f const face_center = (v1 + v2 + v3) / 3.0f;

        /* Check visibility and compute quality */
        //光心到三角形中心连线的两个方向上的向量(世界坐标系中)
        math::Vec3f view_to_face_vec = (face_center - view_pos).normalized();
        math::Vec3f face_to_view_vec = (view_pos - face_center).normalized();

        /* Backface and Basic Frustum Culling */
        float viewing_angle = face_to_view_vec.dot(face_normal);
        if (viewing_angle < 0.0f || //三角面中心到光心连线向量，与 三角面法向的夹角超过90度
            //光轴方向与lookat向量夹角超过90度，这个角在深度相机里面通常不会超过60度(Fov)
            viewing_direction.dot(view_to_face_vec) < 0.0f)
          continue;
        //warn:这里认为法向夹角超过75度的质量不好,直接放弃.正面视图(如果足够正)靠眉角、眼角的地方可能无法通过这个检查.
        if (std::acos(viewing_angle) > MATH_DEG2RAD(75.0f))
          continue;

        /* Projects into the valid part of the TextureView? */
        //Clip Test & Validity Mask Test
        if (!texture_view->inside(v1, v2, v3))
          continue;
        //Occlusion Test
        if (settings.geometric_visibility_test) {
          /* Viewing rays do not collide? */
          bool visible = true;
          math::Vec3f const *samples[] = {&v1, &v2, &v3};
          // TODO: random monte carlo samples...
          for (std::size_t k = 0; k < sizeof(samples) / sizeof(samples[0]);
               ++k) {
            BVHTree::Ray ray;
            ray.origin = *samples[k];
            ray.dir = view_pos - ray.origin;
            ray.tmax = ray.dir.norm();
            ray.tmin = ray.tmax * 0.0001f;
            ray.dir.normalize();

            BVHTree::Hit hit;
            if (bvh_tree.intersect(ray, &hit)) {
              visible = false;
              break;
            }
          }//end for k
          if (!visible)
            continue;
        }
        //开始计算投影信息
        FaceProjectionInfo info = {j, 0.0f, math::Vec3f(0.0f, 0.0f, 0.0f)};
        /* Calculate quality. warn:我们假设所有正常计算的quality都是非负数*/
        texture_view->get_face_info(v1, v2, v3, &info, settings);
        if (info.quality == 0.0) continue;
        if (j==frontal_view_id && isTriangleInFacialFeatureRect(v1,v2,v3,texture_view,featureRectPtr)){
          info.quality=-info.quality;//转为负数，即标记为待提权//>70度的不提权或者只提20%?
          std::cout<<face_id<<" ";
        }
        if (max_qualities[j]<std::abs(info.quality)){
          //最大质量影响到柱状图的全局统计,因此待提权的投影也要算进来
          max_qualities[j]=std::abs(info.quality);
        }

        /* Change color space. 开启outlier check时才有mean_color*/
        mve::image::color_rgb_to_ycbcr(*(info.mean_color));

        std::pair<std::size_t, FaceProjectionInfo> pair(face_id, info);
        projected_face_view_infos.push_back(pair);
      }//end for i<faces.size()

      texture_view->release_image();
      texture_view->release_validity_mask();
      if (settings.data_term == DATA_TERM_GMI) {
        texture_view->release_gradient_magnitude();
      }
      view_counter.inc();
    }//end for j=0; j<num_views;

      // std::sort(projected_face_view_infos.begin(),
      // projected_face_view_infos.end());

//#pragma omp critical
    {//其实如果是串行的，则projected_face_view_infos没有必要，直接往face_projection_infos里面塞即可.
     //face_projection_infos也是个中间产物，为了构造DataCosts稀疏表而设置的.
     //若DataCosts类型支持数据级并行，则本函数可直接对DataCosts进行并行操作.(可依托于MemoryBlock<T>改造它内部实现)
      for (std::size_t i = projected_face_view_infos.size(); 0 < i; --i) {
        std::size_t face_id = projected_face_view_infos[i - 1].first;
        FaceProjectionInfo const &info =
            projected_face_view_infos[i - 1].second;
        face_projection_infos->at(face_id).push_back(info);
      }
      projected_face_view_infos.clear();
    }//end临界区
  }//end omp parallel
  float max_quality=std::numeric_limits<float>::lowest();
  for(float elem:max_qualities){
    max_quality=std::max(elem,max_quality);
  }
  return max_quality;
}
/**
 * 对所有face info进行柱状统计，每个都归一化，并填充到DataCosts表.
 * @param settings
 * @param max_quality
 * @param face_projection_infos
 * @param data_costs
 */
void postprocess_face_infos(Settings const &settings,
                            const float max_quality,
                            FaceProjectionInfos *face_projection_infos,
                            DataCosts *data_costs) {
  using namespace std;
  ProgressCounter face_counter("\tPostprocessing face infos",
                               face_projection_infos->size());
//#pragma omp parallel for schedule(dynamic)
  for (size_t i = 0; i < face_projection_infos->size(); ++i) {
    face_counter.progress<SIMPLE>();
    vector<FaceProjectionInfo> &infos = face_projection_infos->at(i);
    if (settings.outlier_removal != OUTLIER_REMOVAL_NONE) {
      //Photo-Consistency Check!
      photometric_outlier_detection(&infos, settings);
      infos.erase(std::remove_if(infos.begin(), infos.end(),
                      [](FaceProjectionInfo const &info) -> bool {
                        return info.quality == 0.0f; }),
                  infos.end());
    }//end if
    //对(剩余的)投影信息按照视图id进行升序排序,为啥要排序?
    std::sort(infos.begin(), infos.end());
    face_counter.inc();
  }//end for

  Histogram hist_qualities(0.0f, max_quality, 10000);
  for (std::size_t i = 0; i < face_projection_infos->size(); ++i)
    for (FaceProjectionInfo const &info : face_projection_infos->at(i))
      hist_qualities.add_value(std::abs(info.quality));
  //找到99.5%分位的区间上限值
  float clamp_bound = hist_qualities.get_approx_percentile(0.995f);

  /* Calculate the costs. */
  for (std::uint32_t i = 0; i < face_projection_infos->size(); ++i) {
    for (FaceProjectionInfo const &info : face_projection_infos->at(i)) {
      /* Clamp to percentile and normalize.
       * quality处于高位0.5%的投影信息，其标准化的quality为1，其他99.5%的，都针对
       * upper_bound做标准化.
       * 这高位0.5%的投影信息其数据项代价为0，意味着MRF最优化组合中大概率会有它们的存在.
       * 为了验证MRF最后求解出来的最优解宏观上是否都落在柱状图的高位，可以用MRF选
       * 中的data_costs.value求出原始info.quality, 然后去柱状图中计算它所在的区间.
       * */
      float normalized_quality = std::min(1.0f, std::abs(info.quality) / clamp_bound);
      if (info.quality<0){
        normalized_quality=1.f;//简单粗暴提权，直接设置为1.
      }
      float data_cost = (1.0f - normalized_quality);
      data_costs->set_value(i, info.view_id, data_cost);
    }
    /* Ensure that all memory is freeed.fixme:直接clear不就ok了 */
    face_projection_infos->at(i).clear();
  }

  cout << "\tMaximum quality of a face: " << max_quality<<"\n"
      <<"\tNumber of quality:"<<hist_qualities.get_num_values()<<"\n"
     << "\tNormalization Clamping qualities to " << clamp_bound
     <<endl;
}

/**
 * Calculates the data costs for each face and texture view combination,
 * if the face is visible within the texture view.
 * 预先计算每个三角面投影到不同视图下的数据项代价.gmi或area.
 * @param mesh 三角网格
 * @param texture_views 所有视图
 * @param settings 算法各个组件的配置
 * @param[out] data_costs 输出的数据项代价，稀疏表.
 * @param frontal_view_id 正脸视图索引
 * @param featureRectPtr 人脸五官在正脸视图的bbox
 */
void calculate_data_costs(mve::TriangleMesh::ConstPtr mesh,
                          std::vector<TextureView> *texture_views,
                          Settings const &settings,
                          DataCosts *data_costs,
                          std::size_t frontal_view_id,
                          const FacialFeatureRectPtr &featureRectPtr) {

  std::size_t const num_faces = mesh->get_faces().size() / 3;
  std::size_t const num_views = texture_views->size();

  if (num_faces > std::numeric_limits<std::uint32_t>::max())
    throw std::runtime_error("Exeeded maximal number of faces");
  if (num_views > std::numeric_limits<std::uint16_t>::max())
    throw std::runtime_error("Exeeded maximal number of views");

  /*
   * 每个三角面投影到不同视图下的投影信息.
   * 此数组长度等于mesh中三角面数目，每个三角面关联的视图列表长度是不同的!
   * 因为不同的面 与关联的可见视图数目不同.
   *
   * TODO:若data_costs支持并行，则可以将下面两步直接合为一步
   */
  FaceProjectionInfos face_projection_infos(num_faces);
  //先填充face_projection_infos(稠密的?)
  float max_quality=calculate_face_projection_infos(mesh, texture_views,
                                        settings, &face_projection_infos,
                                        frontal_view_id,featureRectPtr);
  //再填充data_costs稀疏表
  postprocess_face_infos(settings, max_quality,&face_projection_infos, data_costs);
}

TEX_NAMESPACE_END

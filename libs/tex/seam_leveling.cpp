/*
 * Copyright (C) 2015, Nils Moehrle
 * TU Darmstadt - Graphics, Capture and Massively Parallel Computing
 * All rights reserved.
 *
 * This software may be modified and distributed under the terms
 * of the BSD 3-Clause license. See the LICENSE.txt file for details.
 */

#include <set>

#include "seam_leveling.h"

TEX_NAMESPACE_BEGIN
/**
 * 在整个mesh中找出所有seam edge.包括与label0邻接的seam edge!
 * 用在local_seam_leveling中.
 * @param graph
 * @param mesh
 * @param seam_edges
 */
void find_seam_edges(UniGraph const &graph,
                     mve::TriangleMesh::ConstPtr mesh,
                     std::vector<MeshEdge> *seam_edges) {
  mve::TriangleMesh::FaceList const &faces = mesh->get_faces();
  seam_edges->clear();
  // Is it possible that a single edge is part of more than three faces whichs'
  // label is non zero??? 在贴纹理之前,非流形边必须干掉.
  for (std::size_t node = 0; node < graph.num_nodes(); ++node) {
    std::vector<std::size_t> const &adj_nodes = graph.get_adj_nodes(node);
    //在UniGraph中每个节点至少有一个邻接节点,至多三个
    for (std::size_t adj_node : adj_nodes) {
      /* Add each edge only once.
       * 与find_seam_edges_for_vertex_label_combination()不同*/
      if (node > adj_node)
        continue;

      int label1 = graph.get_label(node);
      int label2 = graph.get_label(adj_node);
      /* Add only seam edges. */
      if (label1 == label2)
        continue;

      /* Find shared edge of the faces. */
      std::vector<std::size_t> shared_edge;
      for (int i = 0; i < 3; ++i) {
        std::size_t v1 = faces[3 * node + i];

        for (int j = 0; j < 3; j++) {
          std::size_t v2 = faces[3 * adj_node + j];

          if (v1 == v2)
            shared_edge.push_back(v1);
        }
      }

      assert(shared_edge.size() == 2);
      std::size_t v1 = shared_edge[0];
      std::size_t v2 = shared_edge[1];

      assert(v1 != v2);
      if (v1 > v2)
        std::swap(v1, v2);

      MeshEdge seam_edge = {v1, v2};
      seam_edges->push_back(seam_edge);
    }
  }
}

/**
 * 找出seam_edge在相应视图(不同标签)中的投影信息.包括与label0邻接的seam edge.
 * 用在global_seam_leveling()和local_seam_leveling()中.
 *
 * 而此函数内部是从两个端点的VPI列表中每个VPI的faces列表逐两两face比对它们patch id一致、
 * faceid一致才确定了此seam edge的某一个邻接面.
 *
 * todo:在global_seam_leveling()构造A矩阵时，循环中若采用遍历VPI的方式则很容易找出
 * 每条seam edge及邻接面、各自所属patch/标签,进而容易计算出这个seam edge在这两个patch中的VPI.
 *
 * @param vertex_projection_infos 全局VPI
 * @param seam_edge 两个相邻patch(不同标签)之间的某一条边
 * @param [out]edge_projection_infos 返回的边投影信息
 */
void find_mesh_edge_projections(
    std::vector<std::vector<VertexProjectionInfo>> const
        &vertex_projection_infos,
    MeshEdge seam_edge,
    std::vector<EdgeProjectionInfo> *edge_projection_infos) {
  std::vector<VertexProjectionInfo> const &v1_projection_infos =
      vertex_projection_infos[seam_edge.v1];
  std::vector<VertexProjectionInfo> const &v2_projection_infos =
      vertex_projection_infos[seam_edge.v2];

  /* Use a set to eliminate duplicates which may occur if the mesh is
   * degenerated. */
  std::set<EdgeProjectionInfo> edge_projection_infos_set;

  for (VertexProjectionInfo v1_projection_info : v1_projection_infos) {
    for (VertexProjectionInfo v2_projection_info : v2_projection_infos) {
      if (v1_projection_info.texture_patch_id !=
          v2_projection_info.texture_patch_id)
        continue;

      for (std::size_t face_id1 : v1_projection_info.faces) {
        for (std::size_t face_id2 : v2_projection_info.faces) {
          if (face_id1 != face_id2)
            continue;

          std::size_t texture_patch_id = v1_projection_info.texture_patch_id;
          math::Vec2f p1 = v1_projection_info.projection;
          math::Vec2f p2 = v2_projection_info.projection;

          EdgeProjectionInfo edge_projection_info = {texture_patch_id, p1, p2};
          edge_projection_infos_set.insert(edge_projection_info);
        }
      }
    }
  }

  edge_projection_infos->insert(edge_projection_infos->end(),
                                edge_projection_infos_set.begin(),
                                edge_projection_infos_set.end());
}

TEX_NAMESPACE_END

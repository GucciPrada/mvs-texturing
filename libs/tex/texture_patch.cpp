/*
 * Copyright (C) 2015, Nils Moehrle
 * TU Darmstadt - Graphics, Capture and Massively Parallel Computing
 * All rights reserved.
 *
 * This software may be modified and distributed under the terms
 * of the BSD 3-Clause license. See the LICENSE.txt file for details.
 */

#include <set>

#include <math/functions.h>
#include <mve/image_color.h>
#include <mve/image_tools.h>
#include <mve/mesh_io_ply.h>

#include "texture_patch.h"

TEX_NAMESPACE_BEGIN

const uint8_t TexturePatch::BlendMaskValue0=0,
              TexturePatch::BlendMaskValue64=64,
              TexturePatch::BlendMaskValue128=128,
              TexturePatch::BlendMaskValue255=255,
              TexturePatch::ValidMaskValue255=255,
              TexturePatch::ValidMaskValue0=0;

TexturePatch::TexturePatch(int label,
                           std::vector<std::size_t> const &faces,
                           std::vector<math::Vec2f> const &texcoords,
                           mve::FloatImage::Ptr image)
    : label(label), faces(faces), texcoords(texcoords), image(image) {

  validity_mask = mve::ByteImage::create(get_width(), get_height(), 1);
  validity_mask->fill(ValidMaskValue255);
  blending_mask = mve::ByteImage::create(get_width(), get_height(), 1);
}

TexturePatch::TexturePatch(TexturePatch const &texture_patch) {
  label = texture_patch.label;
  faces = std::vector<std::size_t>(texture_patch.faces);
  texcoords = std::vector<math::Vec2f>(texture_patch.texcoords);
  image = texture_patch.image->duplicate();
  validity_mask = texture_patch.validity_mask->duplicate();
  if (texture_patch.blending_mask != NULL) {
    blending_mask = texture_patch.blending_mask->duplicate();
  }
}

const float sqrt_2 = sqrt(2);

#ifdef ZMTEX_PC_DEBUG
#define WND_BLEND_MASK "BlendingMask"
#define WND_VALID_MASK "ValidityMask"
#define WND_PATCH_IMG "PatchImage"
#define WND_ADJ_VALUE "iadjust_value"
#endif

/** 应用global_seam_leveling求解的增益到此patch.
 * @param adjust_values 增益向量.包含此patch内每个顶点的颜色增益值(三个通道).
 *                      最优解g(当label=0时，增量=0).
 **/
void
TexturePatch::adjust_colors(std::vector<math::Vec3f> const & adjust_values) {
  using namespace std;
  assert(blending_mask != NULL);
  validity_mask->fill(ValidMaskValue0);
  #ifdef ZMTEX_PC_DEBUG
    float min_adj_val=numeric_limits<float>::max(),
          max_adj_val=numeric_limits<float>::lowest();
  #endif
  {
  #ifdef ZMTEX_PC_DEBUG
    cv::imshow(WND_BLEND_MASK,blending_mask->toCvMat());
    cv::imshow(WND_VALID_MASK,validity_mask->toCvMat());
    cv::imshow(WND_PATCH_IMG,image->toCvMat());
    cv::waitKey();
  #endif
  };

  /*
   * 1.光栅化当前patch中每个三角形,为三角形中每个纹素计算增量iadjust_value.
   * 2.顺手初始化当前patch的validity_mask和blending_mask:
   * 3.会将patch图像/validity_mask/blending_mask的所有边缘(不规则)扩大1像素,
   *   这1像素边界上的blending_mask为64,validity_mask为255.
   */
  auto iadjust_values = mve::FloatImage::create(get_width(), get_height(), 3);
  for (size_t i = 0; i < texcoords.size(); i += 3) {
    math::Vec2f v1 = texcoords[i];
    math::Vec2f v2 = texcoords[i + 1];
    math::Vec2f v3 = texcoords[i + 2];
    Tri tri(v1, v2, v3);
    float area = tri.get_area();
    if (area < numeric_limits<float>::epsilon()) continue;
    Rect<float> aabb = tri.get_aabb();

    //扩大bbox范围,使patch边缘三角形能纳入patch图像边缘(不规则)像素.(其实每个三角形bbox都扩大了)
    int const min_x = static_cast<int>(floor(aabb.min_x)) - texture_patch_border;
    int const min_y = static_cast<int>(floor(aabb.min_y)) - texture_patch_border;
    int const max_x = static_cast<int>(ceil(aabb.max_x)) + texture_patch_border;
    int const max_y = static_cast<int>(ceil(aabb.max_y)) + texture_patch_border;
    assert(0 <= min_x && max_x <= get_width());
    assert(0 <= min_y && max_y <= get_height());
    for (int y = min_y; y < max_y; ++y) {
      for (int x = min_x; x < max_x; ++x) {

        math::Vec3f bcoords = tri.get_barycentric_coords(x, y);
        bool inside = bcoords.minimum() >= 0.0f;
        if (inside) {
          assert(x != 0 && y != 0);
          for (int c = 0; c < 3; ++c) {
            float adj = math::interpolate(
            adjust_values[i][c], adjust_values[i + 1][c], adjust_values[i + 2][c],
            bcoords[0], bcoords[1], bcoords[2]);
            iadjust_values->at(x, y, c)=adj;
            #ifdef ZMTEX_PC_DEBUG
            min_adj_val=min(min_adj_val,adj); max_adj_val=max(max_adj_val,adj);
            #endif
          }
          validity_mask->at(x, y, 0) = ValidMaskValue255;
          blending_mask->at(x, y, 0) = BlendMaskValue255;
        } else {
          /* 在当前扩大1像素的bbox内发现了此三角形的外边缘像素,假设它就是patch图像边
           * 缘(不规则)的像素,对该像素进行标记:validity_mask标记为255即已被访问,
           * blending_mask标记为64即patch外边缘.这里分两种情况,一是此三角形并不处于
           * patch边缘,这种情况下当对此三角形栅格化时,走到这里,要么发现外边缘像素已被邻接
           * 三角形栅格化访问且validity_mask被抢先标记为255而放弃,因为这些外边缘像素是落
           * 在邻接三角形内部的,要么发现该像素还未被访问则将其标记为已访问且blending_mask
           * 标记为64即作为patch的外边缘像素,等到邻接三角形栅格化时由于该像素走入上面分支
           * 因此其validity_mask取值被忽略且被标记为255,blending_mask直接修改为255即
           * 将此像素标记为patch中的像素而不是patch外边缘像素.
           * 综上,这里要设计成并行计算的话,需要注意validity_mask/blending_mask
           * /iadjust_values这三个对象的并发读写问题,但并发控制有损效率.
           * 说白了这样设计,就是为了计算出patch外边缘.也许换一种做法就能提高这里的并发度.
           * todo:在栅格化时忽略这个分支,事后针对blending_mask再用图像学手段求出外边缘.
           */
          if (validity_mask->at(x, y, 0) == ValidMaskValue255)
            continue;

          /* Check whether the pixels distance from the triangle is more than one pixel. */
          float ha = 2.0f * -bcoords[0] * area / (v2 - v3).norm();
          float hb = 2.0f * -bcoords[1] * area / (v1 - v3).norm();
          float hc = 2.0f * -bcoords[2] * area / (v1 - v2).norm();
          if (ha > sqrt_2 || hb > sqrt_2 || hc > sqrt_2)
            continue;

          for (int c = 0; c < 3; ++c) {
            //patch外边缘像素也要调整亮度.因为它们构成泊松融合的外边界条件.
            float adj = math::interpolate(
            adjust_values[i][c], adjust_values[i + 1][c], adjust_values[i + 2][c],
            bcoords[0], bcoords[1], bcoords[2]);
            iadjust_values->at(x, y, c)=adj;
            #ifdef ZMTEX_PC_DEBUG
            min_adj_val=min(min_adj_val,adj); max_adj_val=max(max_adj_val,adj);
            #endif
          }
          validity_mask->at(x, y, 0) = ValidMaskValue255;
          blending_mask->at(x, y, 0) = BlendMaskValue64;
        }
      }
    }
  }//end for i<texcoords.size()
  {
    #ifdef ZMTEX_PC_DEBUG
    //转换blending_mask
    std::ostringstream oss;
    cv::Mat colorfulBlendMask=getColorfulBlendMask(blending_mask->toCvMat());;
    cv::imshow(WND_BLEND_MASK,colorfulBlendMask);
    oss<<"L"<<label<<"f"<<faces.size()<<"-validMask.png";
    cv::imwrite(oss.str(),validity_mask->toCvMat());
    cv::imshow(WND_VALID_MASK,validity_mask->toCvMat());
    cv::cvtColor(colorfulBlendMask,colorfulBlendMask,CV_BGR2BGRA);
    cv::Mat bgra[4];
    cv::split(colorfulBlendMask,bgra); bgra[3]*=0.7; cv::merge(bgra,4,colorfulBlendMask);
    oss.clear();oss.str("");
    oss<<"L"<<label<<"f"<<faces.size()<<"-colorfulBlendMask.png";
    cv::imwrite(oss.str(),colorfulBlendMask);
    //转换iadjust_value
    cv::Mat colorfulAdjustValue;
    std::cout<<this->faces.size()<<",iadjust_values:"<<min_adj_val<<"~"<<max_adj_val
      <<","<<(min_adj_val*255)<<"~"<<(max_adj_val*255)<<std::endl;
    iadjust_values->toCvMat().convertTo(colorfulAdjustValue,CV_8UC1,255/(max_adj_val-min_adj_val),255*min_adj_val/(min_adj_val-max_adj_val));
    cv::applyColorMap(colorfulAdjustValue,colorfulAdjustValue,cv::COLORMAP_HOT);
    cv::imshow(WND_ADJ_VALUE,colorfulAdjustValue);
    cv::waitKey();
    cv::split(colorfulAdjustValue,bgra); bgra[3]*=0.7; cv::merge(bgra,4,colorfulAdjustValue);
    oss.clear();oss.str("");
    oss<<"L"<<label<<"f"<<faces.size()<<"-AdjustValue.png";
    cv::imwrite(oss.str(),colorfulAdjustValue);
    #endif
  };
  for (int i = 0; i < image->get_pixel_amount(); ++i) {
    //note:patch像素和patch外边缘像素的validity_mask都是255
    if (validity_mask->at(i, 0) != ValidMaskValue0){
      for (int c = 0; c < 3; ++c) {
        image->at(i, c) += iadjust_values->at(i, c);
      }
    } else {
      //note:patch图像中内部孔洞、边界之外的像素全部置为0.
      math::Vec3f color(0.0f, 0.0f, 0.0f);
      //DEBUG math::Vec3f color(1.0f, 0.0f, 1.0f);
      copy(color.begin(), color.end(), &image->at(i, 0));
    }
  }
  {
    #ifdef ZMTEX_PC_DEBUG
    cv::imshow(WND_PATCH_IMG,image->toCvMat());
    cv::waitKey();
    std::ostringstream oss;
    oss<<"L"<<label<<"f"<<faces.size()<<"patchImage.png";
    cv::Mat img;
    image->toCvMat().convertTo(img,CV_8UC3,255);
    cv::imwrite(oss.str(),img);
    /*cv::destroyWindow(WND_VALID_MASK);
    cv::destroyWindow(WND_BLEND_MASK);
    cv::destroyWindow(WND_PATCH_IMG);
    cv::destroyWindow(WND_ADJ_VALUE);*/
    #endif
  };
}

bool TexturePatch::valid_pixel(math::Vec2f pixel) const {
  float x = pixel[0];
  float y = pixel[1];

  float const height = static_cast<float>(get_height());
  float const width = static_cast<float>(get_width());

  bool valid = (0.0f <= x && x < width && 0.0f <= y && y < height);
  if (valid && validity_mask != NULL) {
    /* Only pixel which can be correctly interpolated are valid. */
    float cx = std::max(0.0f, std::min(width - 1.0f, x));
    float cy = std::max(0.0f, std::min(height - 1.0f, y));
    int const floor_x = static_cast<int>(cx);
    int const floor_y = static_cast<int>(cy);
    int const floor_xp1 = std::min(floor_x + 1, get_width() - 1);
    int const floor_yp1 = std::min(floor_y + 1, get_height() - 1);

    float const w1 = cx - static_cast<float>(floor_x);
    float const w0 = 1.0f - w1;
    float const w3 = cy - static_cast<float>(floor_y);
    float const w2 = 1.0f - w3;
    /*参见linear_at*/
    valid =
      (w0 * w2 == 0.0f || validity_mask->at(floor_x, floor_y, 0) == ValidMaskValue255) &&
      (w1 * w2 == 0.0f || validity_mask->at(floor_xp1, floor_y, 0) == ValidMaskValue255) &&
      (w0 * w3 == 0.0f || validity_mask->at(floor_x, floor_yp1, 0) == ValidMaskValue255) &&
      (w1 * w3 == 0.0f || validity_mask->at(floor_xp1, floor_yp1, 0) == ValidMaskValue255);
  }

  return valid;
}

bool TexturePatch::valid_pixel(math::Vec2i pixel) const {
  int const x = pixel[0];
  int const y = pixel[1];

  bool valid = (0 <= x && x < get_width() && 0 <= y && y < get_height());
  if (valid && validity_mask != NULL) {
    valid = validity_mask->at(x, y, 0) == ValidMaskValue255;
  }

  return valid;
}

math::Vec3f TexturePatch::get_pixel_value(math::Vec2f pixel) const {
  assert(valid_pixel(pixel));

  math::Vec3f color;
  image->linear_at(pixel[0], pixel[1], *color);
  return color;
}

void TexturePatch::set_pixel_value(math::Vec2i pixel, math::Vec3f color) {
  assert(blending_mask != NULL);
  assert(valid_pixel(pixel));

  std::copy(color.begin(), color.end(), &image->at(pixel[0], pixel[1], 0));
  blending_mask->at(pixel[0], pixel[1], 0) = BlendMaskValue128;
}

void TexturePatch::blend(mve::FloatImage::ConstPtr orig) {
  poisson_blend(orig, blending_mask, image, 1.0f);

  /* Invalidate all pixels outside the boundary. */
  for (int y = 0; y < blending_mask->height(); ++y) {
    for (int x = 0; x < blending_mask->width(); ++x) {
      if (blending_mask->at(x, y, 0) == BlendMaskValue64) {
        validity_mask->at(x, y, 0) = ValidMaskValue0;
      }
    }
  }
}

typedef std::vector<std::pair<int, int>> PixelVector;
typedef std::set<std::pair<int, int>> PixelSet;

void TexturePatch::prepare_blending_mask(std::size_t strip_width) {
  //mask的宽高与patch bbox一样,也包含border
  int const width = blending_mask->width();
  int const height = blending_mask->height();

  /* Calculate the set of valid pixels at the border of texture patch.
   * 此集合用于保存处于patch边缘的像素坐标.
   * adjust_colors()中已经利用光栅化将patch内所有三角形中像素标记为valid,且处于
   * 边沿三角形(patch外围和内部孔洞)的边缘地带(1像素以内)的像素都标记为valid.
   * */
  PixelSet valid_border_pixels;
  for (int y = 0; y < height; ++y) {
    for (int x = 0; x < width; ++x) {
      if (validity_mask->at(x, y, 0) == ValidMaskValue0)
        continue;

      /* Valid border pixels need no invalid neighbours.
       * 若当前像素是处于bbox边界上的valid像素,则不考察它是否与invalid像素邻接*/
      if (x == 0 || x == width - 1 || y == 0 || y == height - 1) {
        valid_border_pixels.insert(std::pair<int, int>(x, y));
        continue;
      }

      /* Check the direct neighbourhood of all invalid pixels.
       * 检查当前valid像素的[8邻域],若邻域中存在invalid-pixel则将当前像素加入
       * 到valid_border_pixels.此举可以将patch bbox图像中处于边沿的valid像素
       * 都找出来,包括patch外边缘像素和patch内部的孔洞边缘像素.
       * */
      for (int j = -1; j <= 1; ++j) {
        for (int i = -1; i <= 1; ++i) {
          int nx = x + i;
          int ny = y + j;
          /* If the valid pixel has a invalid neighbour: */
          if (validity_mask->at(nx, ny, 0) == ValidMaskValue0) {
            /* Add the pixel to the set of valid border pixels. */
            valid_border_pixels.insert(std::pair<int, int>(x, y));
          }
        }
      }
    }
  }
  //todo:动不动就duplicate()啊
  mve::ByteImage::Ptr inner_pixel = validity_mask->duplicate();

  /* Iteratively erode all border pixels.
   * 一层层地腐蚀边界像素,直到形成了strip*/
  for (std::size_t i = 0; i < strip_width; ++i) {
    PixelVector new_invalid_pixels(valid_border_pixels.begin(),
                                   valid_border_pixels.end());
    PixelVector::iterator it;
    valid_border_pixels.clear();

    /* Mark the new invalid pixels invalid in the validity mask. */
    for (it = new_invalid_pixels.begin(); it != new_invalid_pixels.end();
         ++it) {
      int x = it->first;
      int y = it->second;

      inner_pixel->at(x, y, 0) = 0;
    }

    /* Calculate the set of valid pixels at the border of the valid area. */
    for (it = new_invalid_pixels.begin(); it != new_invalid_pixels.end();
         ++it) {
      int x = it->first;
      int y = it->second;

      for (int j = -1; j <= 1; j++) {
        for (int i = -1; i <= 1; i++) {
          int nx = x + i;
          int ny = y + j;
          if (0 <= nx && nx < width && 0 <= ny && ny < height &&
              inner_pixel->at(nx, ny, 0) == 255) {

            valid_border_pixels.insert(std::pair<int, int>(nx, ny));
          }
        }
      }
    }
  }

  {
    #ifdef ZMTEX_PC_DEBUG
    //转换blending_mask
    std::ostringstream oss;
    cv::Mat colorfulBlendMask=getColorfulBlendMask(blending_mask->toCvMat());
    cv::imshow(WND_BLEND_MASK,colorfulBlendMask);
    oss<<"L"<<label<<"f"<<faces.size()<<"-validMask-erode.png";
    cv::imwrite(oss.str(),inner_pixel->toCvMat());
    cv::imshow(WND_VALID_MASK,inner_pixel->toCvMat());
    cv::waitKey();
    #endif
  }

  /* Sanitize blending mask.再次遍历整张mask.*/
  for (int y = 1; y < height - 1; ++y) {
    for (int x = 1; x < width - 1; ++x) {
      if (blending_mask->at(x, y, 0) == BlendMaskValue128) {
        uint8_t n[] = {
            blending_mask->at(x - 1, y, 0), blending_mask->at(x + 1, y, 0),
            blending_mask->at(x, y - 1, 0), blending_mask->at(x, y + 1, 0)};
        bool valid = true;
        for (uint8_t v : n) {
          //blendmask=128的是patch边线上的像素,周围若全是255,则把它也置为255
          if (v == 255)
            continue;
          valid = false;
        }
        if (valid)
          blending_mask->at(x, y, 0) = BlendMaskValue255;
      }
    }
  }

  {
  #ifdef ZMTEX_PC_DEBUG
    std::ostringstream oss;
    cv::Mat colorfulBlendMask=getColorfulBlendMask(blending_mask->toCvMat());
    cv::imshow("BlendMask>Sanitize",colorfulBlendMask);
    cv::waitKey();
    oss<<"L"<<label<<"f"<<faces.size()<<"-colorfulBlendMask-sanitize.png";
    cv::imwrite(oss.str(),blending_mask->toCvMat());
  #endif
  }

  /* Mark all remaining pixels invalid in the blending_mask.
   * 把内部圆的blendingMask都置为0*/
  for (int i = 0; i < inner_pixel->get_pixel_amount(); ++i) {
    if (inner_pixel->at(i) == 255)
      blending_mask->at(i) = BlendMaskValue0;
  }

  {
  #ifdef ZMTEX_PC_DEBUG
    std::ostringstream oss;
    cv::Mat colorfulBlendMask=getColorfulBlendMask(blending_mask->toCvMat());
    cv::imshow("BlendMask>InnerBlack",colorfulBlendMask);
    cv::waitKey();
    oss<<"L"<<label<<"f"<<faces.size()<<"-colorfulBlendMask-innerblack.png";
    cv::imwrite(oss.str(),blending_mask->toCvMat());
  #endif
  }

  /* Mark all border pixels. valid_border_pixels保存的是最内层的圆环边线,在innerPixel
   * 中还是白色*/
  PixelSet::iterator it;
  for (it = valid_border_pixels.begin(); it != valid_border_pixels.end(); ++it) {
    int x = it->first;
    int y = it->second;
    blending_mask->at(x, y, 0) = BlendMaskValue128;
  }

  {
  #ifdef ZMTEX_PC_DEBUG
    std::ostringstream oss;
    cv::Mat colorfulBlendMask=getColorfulBlendMask(blending_mask->toCvMat());
    cv::imshow("BlendMask>InnerBorder",colorfulBlendMask);
    cv::waitKey();
    oss<<"L"<<label<<"f"<<faces.size()<<"-colorfulBlendMask-innerBorder.png";
    cv::imwrite(oss.str(),blending_mask->toCvMat());
  #endif
  }
}

TEX_NAMESPACE_END


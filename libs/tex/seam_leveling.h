/*
 * Copyright (C) 2015, Nils Moehrle
 * TU Darmstadt - Graphics, Capture and Massively Parallel Computing
 * All rights reserved.
 *
 * This software may be modified and distributed under the terms
 * of the BSD 3-Clause license. See the LICENSE.txt file for details.
 */

#ifndef TEX_SEAMLEVELING_HEADER
#define TEX_SEAMLEVELING_HEADER

#include <vector>

#include <mve/mesh.h>

#include "defines.h"
#include "uni_graph.h"

TEX_NAMESPACE_BEGIN
/**
 * 顶点投影信息:
 *
 * 1. 此顶点所属的texture patch id, 间接关联上了视图id
 *
 * 2. 此顶点投影到对应视图后的相对坐标，以patch的图像左上角为原点
 *
 * 3. 关联的联通子图
 *
 * 若参与排序，顶点投影信息是按照texture patch id升序排序.
 */
struct VertexProjectionInfo {
  std::size_t texture_patch_id;///<patch id，从0开始,可作为TexturePatches索引
  math::Vec2f projection;
  std::vector<std::size_t> faces;

  bool operator<(VertexProjectionInfo const &other) const {
    return texture_patch_id < other.texture_patch_id;
  }
};

/**
 * 边的投影信息:
 *
 * 1.texture patch id，间接关联上了视图id
 *
 * 2.投影后，端点p1的平面坐标
 *
 * 3.投影后，端点p2的平面坐标
 *
 * 按照texture patch id升序排序.
 */
struct EdgeProjectionInfo {
  std::size_t texture_patch_id;
  math::Vec2f p1;
  math::Vec2f p2;

  bool operator<(EdgeProjectionInfo const &other) const {
    return texture_patch_id < other.texture_patch_id;
  }
};

/**
 * mesh上的边:包含两个端点的vid
 */
struct MeshEdge {
  std::size_t v1;///<这条边上某一个顶点的vid
  std::size_t v2;///<这条边上另一个顶点的vid
};

/**
 * 用来取代MeshEdge在global_seam_leveling中的作用.
 * @warning 该类型只能用于描述流形边且非边界边！实际上,输入的mesh中如果包含非流形成分,
 * 会带来意想不到的结果,代码中很多地方都假设mesh是流形.
 */
struct SeamEdge{
  std::size_t v1;///<这条seam edge的某个顶点vid
  std::size_t v2;///<这条seam edge的另一个顶点vid

  std::size_t f1;///<这条seam edge某一个邻接面
  std::size_t f2;///<这条seam edge的另一个邻接面.
  SeamEdge(std::size_t v1,std::size_t v2,
    std::size_t f1,std::size_t f2):v1(v1),v2(v2),f1(f1),f2(f2){
  }
};

void find_seam_edges(UniGraph const &graph,
                     mve::TriangleMesh::ConstPtr mesh,
                     std::vector<MeshEdge> *seam_edges);

void find_mesh_edge_projections(
    std::vector<std::vector<VertexProjectionInfo>> const
        &vertex_projection_infos,
    MeshEdge seam_edge,
    std::vector<EdgeProjectionInfo> *projected_edge_infos);

TEX_NAMESPACE_END

#endif /* TEX_SEAMLEVELING_HEADER */

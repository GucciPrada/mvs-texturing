find -E . -regex ".*\.(h|m|mm|tpp|hpp|cpp|cc)" | xargs clang-format -i -style=file 
#clang-format程序的-assume-filename参数的作用是：当源代码是从标准输入输入到clang-format程序时，
#或者源代码所在的源文件没有后缀时，指定-assume-filename=test.cpp可以让clang-format以为当前输
#入的源码内容是c++，若指定test.js，则会被认定为javascript，诸如此类.
#当指定-style=file参数时，clang-format会默认在当前工作目录或所有上级目录搜索名字为.clang-format或_clang_format的配置文件

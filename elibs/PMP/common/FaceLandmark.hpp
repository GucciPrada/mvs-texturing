//
// Created by fisher on 2018/5/24.
//

#ifndef INFINITAM_FACELANDMARK_HPP
#define INFINITAM_FACELANDMARK_HPP

#ifndef __OBJC__
#include <opencv2/opencv.hpp>
#endif

namespace FaceLandmark {

/**************** 特征点数量及其他常量 ******************/
extern const int LANDMARK_COUNT;
extern const int LANDMARK_FRONT_COUNT;
extern const int LANDMARK_LEFT_COUNT;
extern const int LANDMARK_RIHT_COUNT;
extern const int LANDMARK_LEFT_CHECK_START;
extern const int LANDMARK_LEFT_CHECK_END;
extern const int LANDMARK_RIGHT_CHECK_START;
extern const int LANDMARK_RIGHT_CHECK_END;
// 五官点数量
extern const int FEATURES_COUNT;
// 五官起始坐标
extern const int FEATURES_START;

/**************** 脸部类型（对应ldmk3d.yml类型） ******************/
extern const int FRONT_FACE;
extern const int LEFT_FACE;
extern const int RIGHT_FACE;

/**************** 脸颊 ******************/
extern const int CHEEK_LEFT_1;
extern const int CHEEK_LEFT_2;
extern const int CHEEK_LEFT_3;
extern const int CHEEK_LEFT_4;
extern const int CHEEK_LEFT_5;
extern const int CHEEK_LEFT_6;
extern const int CHEEK_LEFT_7;
extern const int CHEEK_LEFT_8;
extern const int CHEEK_CENTER;
extern const int CHEEK_RIGHT_1;
extern const int CHEEK_RIGHT_2;
extern const int CHEEK_RIGHT_3;
extern const int CHEEK_RIGHT_4;
extern const int CHEEK_RIGHT_5;
extern const int CHEEK_RIGHT_6;
extern const int CHEEK_RIGHT_7;
extern const int CHEEK_RIGHT_8;

/**************** 眉毛 ******************/
extern const int EYEBROW_LEFT_1;
extern const int EYEBROW_LEFT_2;
extern const int EYEBROW_LEFT_3;
extern const int EYEBROW_LEFT_4;
extern const int EYEBROW_LEFT_5;
extern const int EYEBROW_RIHGT_1;
extern const int EYEBROW_RIHGT_2;
extern const int EYEBROW_RIHGT_3;
extern const int EYEBROW_RIHGT_4;
extern const int EYEBROW_RIHGT_5;

/**************** 眼睛 ******************/
extern const int EYE_LEFT_1;
extern const int EYE_LEFT_2;
extern const int EYE_LEFT_3;
extern const int EYE_LEFT_4;
extern const int EYE_LEFT_5;
extern const int EYE_LEFT_6;
extern const int EYE_RIGHT_1;
extern const int EYE_RIGHT_2;
extern const int EYE_RIGHT_3;
extern const int EYE_RIGHT_4;
extern const int EYE_RIGHT_5;
extern const int EYE_RIGHT_6;

/**************** 瞳孔 ******************/
extern const int PUPIL_LEFT;
extern const int PUPIL_RIGHT;

/**************** 鼻子 ******************/
extern const int NOSE_1;
extern const int NOSE_2;
extern const int NOSE_3;
extern const int NOSE_4;
extern const int NOSE_5;
extern const int NOSE_6;
extern const int NOSE_7;
extern const int NOSE_8;
extern const int NOSE_9;

/**************** 嘴巴 ******************/
extern const int MOUTH_EXTERNAL_1;
extern const int MOUTH_EXTERNAL_2;
extern const int MOUTH_EXTERNAL_3;
extern const int MOUTH_EXTERNAL_4;
extern const int MOUTH_EXTERNAL_5;
extern const int MOUTH_EXTERNAL_6;
extern const int MOUTH_EXTERNAL_7;
extern const int MOUTH_EXTERNAL_8;
extern const int MOUTH_EXTERNAL_9;
extern const int MOUTH_EXTERNAL_10;
extern const int MOUTH_EXTERNAL_11;
extern const int MOUTH_EXTERNAL_12;
extern const int MOUTH_INNER_1;
extern const int MOUTH_INNER_2;
extern const int MOUTH_INNER_3;
extern const int MOUTH_INNER_4;
extern const int MOUTH_INNER_5;
extern const int MOUTH_INNER_6;
extern const int MOUTH_INNER_7;
extern const int MOUTH_INNER_8;

/**************** 脸颊（左侧侧脸） ******************/
extern const int LEFT_CHEEK_1;
extern const int LEFT_CHEEK_2;
extern const int LEFT_CHEEK_3;
extern const int LEFT_CHEEK_4;
extern const int LEFT_CHEEK_5;
extern const int LEFT_CHEEK_6;
extern const int LEFT_CHEEK_7;
extern const int LEFT_CHEEK_8;

/**************** 脸颊（右侧侧脸） ******************/
extern const int RIGHT_CHEEK_1;
extern const int RIGHT_CHEEK_2;
extern const int RIGHT_CHEEK_3;
extern const int RIGHT_CHEEK_4;
extern const int RIGHT_CHEEK_5;
extern const int RIGHT_CHEEK_6;
extern const int RIGHT_CHEEK_7;
extern const int RIGHT_CHEEK_8;

/**
 * 功能描述: 获取三维特顶点下标
 *
 * @param landmark3D  三维特征点下标索引（对应ldmk3d.yml数据）
 * @param faceType    脸部类型（0：正脸 1：左脸（侧脸模式） 2：右脸（侧脸模式））
 * @param index       face++索引
 * @return int 三维特顶点下标
 */
int getVertexIndex(const std::vector<cv::Mat> landmark3D, int faceType, int index);

/**
 * 功能描述: 获取三维特顶点下标（正脸）
 *
 * @param landmark3D  三维特征点下标索引（对应ldmk3d.yml数据）
 * @param index       face++索引
 * @return int 三维特顶点下标
 */
int getFrontVertexIndex(const std::vector<cv::Mat> landmark3D, int index);

/**
 * 功能描述: 获取三维特顶点下标（侧脸左脸）
 *
 * @param landmark3D  三维特征点下标索引（对应ldmk3d.yml数据）
 * @param index       face++索引
 * @return int 三维特顶点下标
 */
int getLeftVertexIndex(const std::vector<cv::Mat> landmark3D, int index);

/**
 * 功能描述: 获取三维特顶点下标（侧脸右脸）
 *
 * @param landmark3D  三维特征点下标索引（对应ldmk3d.yml数据）
 * @param index       face++索引
 * @return int 三维特顶点下标
 */
int getRightVertexIndex(const std::vector<cv::Mat> landmark3D, int index);


};


#endif //INFINITAM_FACELANDMARK_H

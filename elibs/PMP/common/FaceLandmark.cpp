//
// Created by fisher on 2018/5/24.
//

#include "FaceLandmark.hpp"

namespace FaceLandmark {

/**************** 特征点数量及其他常量 ******************/
extern const int LANDMARK_COUNT = 68;
extern const int LANDMARK_FRONT_COUNT = 68;
extern const int LANDMARK_LEFT_COUNT = 8;
extern const int LANDMARK_RIHT_COUNT = 8;
extern const int LANDMARK_LEFT_CHECK_START = 0;
extern const int LANDMARK_LEFT_CHECK_END = 7;
extern const int LANDMARK_RIGHT_CHECK_START = 9;
extern const int LANDMARK_RIGHT_CHECK_END = 16;
extern const int FEATURES_COUNT = 51; // 对于104个点版本则为71
extern const int FEATURES_START = 17; // 对于104个点版本则为33

/**************** 脸部类型（对应ldmk3d.yml类型） ******************/
extern const int FRONT_FACE = 0;
extern const int RIGHT_FACE = 1;
extern const int LEFT_FACE = 2;

/**************** 脸颊 ******************/
extern const int CHEEK_LEFT_1 = 0;
extern const int CHEEK_LEFT_2 = 1;
extern const int CHEEK_LEFT_3 = 2;
extern const int CHEEK_LEFT_4 = 3;
extern const int CHEEK_LEFT_5 = 4;
extern const int CHEEK_LEFT_6 = 5;
extern const int CHEEK_LEFT_7 = 6;
extern const int CHEEK_LEFT_8 = 7;
extern const int CHEEK_CENTER = 8;
extern const int CHEEK_RIGHT_1 = 9;
extern const int CHEEK_RIGHT_2 = 10;
extern const int CHEEK_RIGHT_3 = 11;
extern const int CHEEK_RIGHT_4 = 12;
extern const int CHEEK_RIGHT_5 = 13;
extern const int CHEEK_RIGHT_6 = 14;
extern const int CHEEK_RIGHT_7 = 15;
extern const int CHEEK_RIGHT_8 = 16;

/**************** 眉毛 ******************/
extern const int EYEBROW_LEFT_1 = 17;
extern const int EYEBROW_LEFT_2 = 18;
extern const int EYEBROW_LEFT_3 = 19;
extern const int EYEBROW_LEFT_4 = 20;
extern const int EYEBROW_LEFT_5 = 21;
extern const int EYEBROW_RIHGT_1 = 22;
extern const int EYEBROW_RIHGT_2 = 23;
extern const int EYEBROW_RIHGT_3 = 24;
extern const int EYEBROW_RIHGT_4 = 25;
extern const int EYEBROW_RIHGT_5 = 26;

/**************** 眼睛 ******************/
extern const int EYE_LEFT_1 = 36;
extern const int EYE_LEFT_2 = 37;
extern const int EYE_LEFT_3 = 38;
extern const int EYE_LEFT_4 = 39;
extern const int EYE_LEFT_5 = 40;
extern const int EYE_LEFT_6 = 41;
extern const int EYE_RIGHT_1 = 42;
extern const int EYE_RIGHT_2 = 43;
extern const int EYE_RIGHT_3 = 44;
extern const int EYE_RIGHT_4 = 45;
extern const int EYE_RIGHT_5 = 46;
extern const int EYE_RIGHT_6 = 47;

/**************** 鼻子 ******************/
extern const int NOSE_1 = 27;
extern const int NOSE_2 = 28;
extern const int NOSE_3 = 29;
extern const int NOSE_4 = 30;
extern const int NOSE_5 = 31;
extern const int NOSE_6 = 32;
extern const int NOSE_7 = 33;
extern const int NOSE_8 = 34;
extern const int NOSE_9 = 35;

/**************** 嘴巴 ******************/
extern const int MOUTH_EXTERNAL_1 = 48;
extern const int MOUTH_EXTERNAL_2 = 49;
extern const int MOUTH_EXTERNAL_3 = 50;
extern const int MOUTH_EXTERNAL_4 = 51;
extern const int MOUTH_EXTERNAL_5 = 52;
extern const int MOUTH_EXTERNAL_6 = 53;
extern const int MOUTH_EXTERNAL_7 = 54;
extern const int MOUTH_EXTERNAL_8 = 55;
extern const int MOUTH_EXTERNAL_9 = 56;
extern const int MOUTH_EXTERNAL_10 = 57;
extern const int MOUTH_EXTERNAL_11 = 58;
extern const int MOUTH_EXTERNAL_12 = 59;
extern const int MOUTH_INNER_1 = 60;
extern const int MOUTH_INNER_2 = 61;
extern const int MOUTH_INNER_3 = 62;
extern const int MOUTH_INNER_4 = 63;
extern const int MOUTH_INNER_5 = 64;
extern const int MOUTH_INNER_6 = 65;
extern const int MOUTH_INNER_7 = 66;
extern const int MOUTH_INNER_8 = 67;

/**************** 脸颊（左侧侧脸） ******************/
extern const int LEFT_CHEEK_1 = 0;
extern const int LEFT_CHEEK_2 = 1;
extern const int LEFT_CHEEK_3 = 2;
extern const int LEFT_CHEEK_4 = 3;
extern const int LEFT_CHEEK_5 = 4;
extern const int LEFT_CHEEK_6 = 5;
extern const int LEFT_CHEEK_7 = 6;
extern const int LEFT_CHEEK_8 = 7;

/**************** 脸颊（右侧侧脸） ******************/
extern const int RIGHT_CHEEK_1 = 0;
extern const int RIGHT_CHEEK_2 = 1;
extern const int RIGHT_CHEEK_3 = 2;
extern const int RIGHT_CHEEK_4 = 3;
extern const int RIGHT_CHEEK_5 = 4;
extern const int RIGHT_CHEEK_6 = 5;
extern const int RIGHT_CHEEK_7 = 6;
extern const int RIGHT_CHEEK_8 = 7;

/**
 * 功能描述: 获取三维特顶点下标
 *
 * @param landmark3D  三维特征点下标索引（对应ldmk3d.yml数据）
 * @param faceType    脸部类型（0：正脸 1：左脸（侧脸模式） 2：右脸（侧脸模式））
 * @param index       face++索引
 * @return int 三维特顶点下标
 */
int getVertexIndex(const std::vector<cv::Mat> landmark3D, int faceType, int index) {
  return landmark3D[faceType].at<int>(0, index);
};

/**
 * 功能描述: 获取三维特顶点下标（正脸）
 *
 * @param landmark3D  三维特征点下标索引（对应ldmk3d.yml数据）
 * @param index       face++索引
 * @return int 三维特顶点下标
 */
int getFrontVertexIndex(const std::vector<cv::Mat> landmark3D, int index) {
  return getVertexIndex(landmark3D, FRONT_FACE, index);
};

/**
 * 功能描述: 获取三维特顶点下标（侧脸左脸）
 *
 * @param landmark3D  三维特征点下标索引（对应ldmk3d.yml数据）
 * @param index       face++索引
 * @return int 三维特顶点下标
 */
int getLeftVertexIndex(const std::vector<cv::Mat> landmark3D, int index) {
  return getVertexIndex(landmark3D, LEFT_FACE, index);
};

/**
 * 功能描述: 获取三维特顶点下标（侧脸右脸）
 *
 * @param landmark3D  三维特征点下标索引（对应ldmk3d.yml数据）
 * @param index       face++索引
 * @return int 三维特顶点下标
 */
int getRightVertexIndex(const std::vector<cv::Mat> landmark3D, int index) {
  return getVertexIndex(landmark3D, RIGHT_FACE, index);
};


};

//
//  main.m
//  texreconMac
//
//  Created by 程龙 on 2018/8/11.
//

#import <Foundation/Foundation.h>
#include <Accelerate/Accelerate.h>
#include "zmtex.h"

int main(int argc, const char * argv[]) {
  @autoreleasepool {
    // insert code here...
    for(int i=0; i<argc; i++){
      std::cout<<argv[i]<<" ";
    }
    std::cout<<std::endl;
    texrecon(argc,(const char **)argv);
  }
  return 0;
}

